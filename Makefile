$(shell mkdir -p man/man1)

all: man/man1/miniml.1.gz man/man1/miniml-fetch.1.gz

man/man1/%.1.gz: man/rst/%.rst
	rst2man $^ | gzip > $@
