#include <cassert>

#include <zlib.h>

#include <wrenlab/compression/string.hpp>

namespace wrenlab {

std::string
compress_string(const std::string& s) {
    std::string o;
    size_t ratio = 2;

    z_stream z;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * 2;
        o.resize(size);

        z.avail_in = s.length();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = size;
        z.next_out = (unsigned char*) &o[0]; 

        assert (deflateInit(&z, Z_BEST_SPEED) == Z_OK);
        deflate(&z, Z_FINISH);
        deflateEnd(&z);

        //ratio *= round(s.length() / z.avail_in) + 1;
        ratio *= 2;
        o.resize(size - z.avail_out);
    } while (z.avail_in != 0);

    return o;
}

std::string
decompress_string(const std::string& s) {
    size_t ratio = 1;

    std::string o;
    size_t avail_in;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z_stream z;
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * ratio;
        o.resize(size);

        z.avail_in = s.size();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = o.size();
        z.next_out = (unsigned char*) &o[0];
         
        assert(inflateInit(&z) == Z_OK);
        inflate(&z, Z_NO_FLUSH);
        inflateEnd(&z);

        //std::cerr << s.length() << "\t" << z.avail_in << std::endl;
        //ratio *= round(s.length() / z.avail_in) + 1;
        //std::cerr << "factor: " << round(s.length() / z.avail_in) + 1 << std::endl;
        ratio *= 2;
        o.resize(size - z.avail_out);	
        avail_in = z.avail_in;
    } while (avail_in != 0);

    return o;
};

};
