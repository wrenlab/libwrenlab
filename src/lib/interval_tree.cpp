#include <wrenlab/ext/interval_tree.hpp>
#include <interval_tree.h>

#include <iostream>

typedef IntervalTree<void*, uint64_t> CXXIntervalTree;
typedef Interval<void*, uint64_t> CXXInterval;

itree* 
itree_new(itree_interval* intervals, size_t n) {
    std::vector<CXXInterval> v;
    for (size_t i=0; i<n; i++) {
        itree_interval* iv = intervals + i;
        CXXInterval cxxiv = CXXInterval(iv->start, iv->end, iv->data);
        v.push_back(cxxiv);
    }
    CXXIntervalTree* tree = new CXXIntervalTree(v);
    return tree;
}

void
itree_delete(itree* tree) {
    CXXIntervalTree* cxxtree = reinterpret_cast<CXXIntervalTree*>(tree);
    delete cxxtree;
}

itree_match*
itree_search_overlapping(itree* tree, uint64_t start, uint64_t end) {
    std::vector<CXXInterval>* o = new std::vector<CXXInterval>();
    CXXIntervalTree* cxxtree = reinterpret_cast<CXXIntervalTree*>(tree);
    cxxtree->findOverlapping(start, end, *o);
    return reinterpret_cast<itree_match*>(o);
}

itree_match*
itree_search_contained(itree* tree, uint64_t start, uint64_t end) {
    std::vector<CXXInterval>* o = new std::vector<CXXInterval>();
    CXXIntervalTree* cxxtree = reinterpret_cast<CXXIntervalTree*>(tree);
    cxxtree->findContained(start, end, *o);
    return reinterpret_cast<itree_match*>(o);
}

void
itree_match_delete(itree_match* match) {
    std::vector<CXXInterval>* cxxmatch = reinterpret_cast<std::vector<CXXInterval>* >(match);
    delete cxxmatch;
}

size_t 
itree_match_size(itree_match* match) {
    std::vector<CXXInterval>* cxxmatch = reinterpret_cast<std::vector<CXXInterval>* >(match);
    return cxxmatch->size();
}

itree_interval
itree_match_element(itree_match* match, size_t index) {
    std::vector<CXXInterval>* cxxmatch = reinterpret_cast<std::vector<CXXInterval>* >(match);
    CXXInterval cxxiv = (*cxxmatch)[index];
    itree_interval o = {cxxiv.start, cxxiv.stop, cxxiv.value};
    return o;
}
