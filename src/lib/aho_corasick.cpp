#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include <sstream>
#include <memory>
#include <algorithm>

#include <wrenlab/aho_corasick.hpp>

namespace wrenlab {
namespace AhoCorasick {

using namespace std;

void 
Node::build() {
  std::shared_ptr<Node> target = parent->fail;
  while (true) {
    if (target->find(content) != NULL) {
      this->fail = target->find(content);
      return;
    } else if (target->depth() == 0) {
      this->fail = target;
      return;
    } else {
      target = target->fail;
    }
  }
}

int 
Node::depth() {
	if (parent == NULL)
		return 0;
	return parent->depth() + 1;
}

shared_ptr<Node>
Node::find(char c) {
	for (int i=0; i<children.size(); i++) {
		if (children[i]->content == c)
			return children[i];
	}
	return NULL;
};

std::shared_ptr<Node>
Node::search(char c) {
  std::shared_ptr<Node> result = find(c);
  if (result != NULL)
    return result;
  if (depth() == 0) {
    // root's fail points to itself
    return this->fail;
  }
  return this->fail->search(c);
}

std::shared_ptr<Node>
Node::find_or_fail(char c) {
	shared_ptr<Node> result = find(c);
	if (result == NULL) {
		return fail;
	} else {
		return result;
	}
}

void 
Trie::add(string id, string s) {
	shared_ptr<Node> current = root;
	if (!case_sensitive)
		transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i=0; i<s.length(); i++) {
		shared_ptr<Node> child = current->find(s[i]);
		if (child == NULL) {
			shared_ptr<Node> c(new Node(s[i]));
			child = c;
			current->children.push_back(child);
			child->parent = current;
		}
		current = child;
	}
	current->terminal = true;
	current->id = id;
  size++;
}

/*
void Trie::add_fail_transitions(shared_ptr<Node> node) {
	vector<shared_ptr<Node> > children = node->children;
	shared_ptr<Node> child;
	shared_ptr<Node> parent_tsn;
	for (int i=0; i<children.size(); i++) {
		child = children[i];
		if (node == root) {
			child->fail = root;
		} else {
			parent_tsn = node->fail->find(child->content);
			if (parent_tsn == NULL)
				child->fail = root;
			else
				child->fail = parent_tsn;
		}
		add_fail_transitions(child);
	}
}
*/

void Trie::build() {
  node_vector nodes = root->descendants();
  nodes.push_back(root);
  std::sort(nodes.begin(), nodes.end(), node_ptr_comparator());
  for (std::shared_ptr<Node> node : nodes) {
    if (node->depth() <= 1) {
      node->fail = root;
    } else {
      node->build();
    }
  }
}

/*
std::set<size_t> 
word_boundaries(const std::string& s) {
  std::set<size_t> b;
	for (size_t i=0; i<s.size()-1; i++)
		if (s[i]==' ' && s[i+1]!=' ')
			b.insert(i);	
	return b;
}
*/

std::vector<Match>
Trie::search(string s) {
  std::vector<Match> matches;
  if (size == 0)
    return matches;
	if (s.size() == 0)
		return matches;

	if (!case_sensitive)
		transform(s.begin(), s.end(), s.begin(), ::tolower);

  std::shared_ptr<Node> current = root;	
	for (int i=0; i<s.length(); i++) {
    current = current->search(s[i]);
		if (current->terminal) {
      std::shared_ptr<Node> n = current;
      while (n->terminal) {
        Match match;
        match.id = current->id;
        match.start = i + 1 - current->depth();
        match.end = i;
        matches.push_back(match);
        n = n->fail;
      }
    }
	}

  if (!boundary_characters.empty()) {
    std::vector<Match> new_matches;
    std::set<size_t> boundaries;
    for (size_t i=0; i<s.size(); i++) {
      if (boundary_characters.find(s[i]) != boundary_characters.end())
        boundaries.insert(i);
    }

    for (Match match : matches) {
      if ((match.start != 0) && (boundaries.find(match.start - 1) == boundaries.end())) {
        continue;
      }
      if ((match.end != s.size() - 1) && (boundaries.find(match.end + 1) == boundaries.end())) {
        continue;
      }
      new_matches.push_back(match);
    }
    matches = new_matches;
  }

  if (remove_overlaps) {
    matches = longest_nonoverlapping_matches(matches);
  }
	return matches;
}

struct match_length_key {
	inline bool operator() (const Match& m1, const Match& m2) {
		return m1.length() > m2.length();
	}
};

std::vector<Match>
longest_nonoverlapping_matches(std::vector<Match>& v) {
	sort(v.begin(), v.end(), match_length_key());
	vector<Match> out;
	for (Match m1 : v) {
		bool add = true;
		for (Match m2 : out)
			if (m1.overlaps(m2)) {
				add = false;
      }
		if (add) {
			out.push_back(m1);
    }
	}
	return out;
}

}
}
