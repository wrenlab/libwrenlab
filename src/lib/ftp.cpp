#include <cstdio>

#include <iostream>

#include <wrenlab/ftp.hpp>
#include <wrenlab/log.hpp>
#include <wrenlab/util.hpp>

namespace wrenlab {

struct MemoryStruct {
  char *memory;
  size_t size;
};
 
static size_t
write_memory(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    mem->memory = (char*) realloc((void*) mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
        /* out of memory! */ 
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

std::vector<FTPDirectoryEntry>
FTPClient::ls(std::string subdir) {
    MemoryStruct chunk = {(char*) malloc(1), 0};

    if (subdir[0] != '/') {
        subdir = "/" + subdir;
    }
    std::string url = "ftp://" + host + subdir;
    LOG()->info("Listing FTP directory: {0}", url);

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &chunk);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "MLSD");
    int rc = curl_easy_perform(curl);

    if (rc != CURLE_OK) {
        wrenlab::LOG()->error("Error retrieving URL: {0}", url);
        free(chunk.memory);
        throw FTPError();
    }
    std::string output((char*) chunk.memory, chunk.size);
    free(chunk.memory);

    std::vector<FTPDirectoryEntry> o;
    for (auto line : wrenlab::split(output, '\n')) {
        FTPDirectoryEntry entry;
        if (line.size() == 0)
            break;
        std::vector<std::string> fields = wrenlab::split(line, ';');
        for (size_t i=0; i<fields.size() - 1; i++) {
            std::vector<std::string> kv = wrenlab::split(fields[i], '=');
            entry.attributes[kv[0]] = kv[1];
        }
        std::string name = fields[fields.size() - 1];
        entry.name = name.substr(1, name.size());
        entry.directory = subdir;
        o.push_back(entry);
    }
    return o;
}

};
