#include <algorithm>
#include <sstream>
#include <cmath>

#include <wrenlab/ncbi/geo/miniml.hpp>
#include <wrenlab/log.hpp>
#include <wrenlab/ftp.hpp>
#include <wrenlab/aspera.hpp>

namespace wrenlab {
namespace ncbi {
namespace geo {

/* MiniML parse functions */

wrenlab::XMLDocument
parse_xml(std::string xml) {
    bool verbose = false;
    std::istringstream xml_input(xml);
    wrenlab::XMLDocument doc;
    doc.parse(xml_input);
    return doc;
}

column_map_t
parse_column_map(wrenlab::XMLNode& node) {
  column_map_t o;
  if (!node.has_child("Data-Table")) {
      return o;
  }
  wrenlab::XMLNode table = node.get_children("Data-Table")[0];
  for (auto c : table.get_children("Column")) {
      size_t position = atof(c.attributes()["position"].c_str());
      std::string name(c.get_children("Name")[0].text());
      o[name] = position - 1;
  }
  return o;
}

std::unordered_map<std::string, column_map_t>
parse_sample_column_map(wrenlab::XMLDocument& doc) {
    std::unordered_map<std::string, column_map_t> o;

    wrenlab::xml_nsmap_t nsmap;
    nsmap["miniml"] = "http://www.ncbi.nlm.nih.gov/geo/info/MINiML";

    std::string expression = "/miniml:MINiML/miniml:Sample";
    wrenlab::XPath xpath = doc.xpath(expression, nsmap);
    std::vector<wrenlab::XMLNode> nodes = xpath.nodes();

    for (auto node : nodes) {
        std::string accession = node.get_attribute("iid");
        if (accession.size() == 0) {
            continue;
        }
        column_map_t cm = parse_column_map(node);
        if (cm.size() > 0) {
          o[accession] = cm;
        }
    }
    return o;
}

column_map_t
parse_platform_column_map(wrenlab::XMLDocument& doc) {
    wrenlab::xml_nsmap_t nsmap;
    nsmap["miniml"] = "http://www.ncbi.nlm.nih.gov/geo/info/MINiML";

    std::string expression = "/miniml:MINiML/miniml:Platform";
    wrenlab::XPath xpath = doc.xpath(expression, nsmap);
    wrenlab::XMLNode node = xpath.nodes()[0];
    return parse_column_map(node);
}

std::vector<std::string>
column_map_to_vector(column_map_t cm) {
  size_t nc = 0;
  for (auto kv : cm) {
    nc = std::max(nc, kv.second);
  }
  std::vector<std::string> o;
  o.resize(nc+1);
  for (auto kv : cm) {
    o[kv.second] = kv.first;
  }
  return o;
}

void 
MiniML::initialize() {
    read_next();
    accession = split(current_name(), '_')[0];
    LOG()->info("Parsing MiniML archive for {0}...", accession);
    std::string xml = data();
    wrenlab::XMLDocument doc = parse_xml(xml);

    LOG()->info("Parsing MiniML column maps.");
    column_maps = parse_sample_column_map(doc);
    LOG()->info("Column map obtained for {0:d} accessions.", column_maps.size());
    
    auto ptbl_cmap = parse_platform_column_map(doc);
    LOG()->info("cmap: {}", ptbl_cmap.size());
    auto ptbl_columns = column_map_to_vector(ptbl_cmap);
    LOG()->info("Platform table parsed with {} columns.", ptbl_columns.size());
    _platform_table.set_columns(ptbl_columns);
    
    read_next();
    std::vector<std::string> lines = data_lines();
    for (size_t i=0; i<lines.size(); i++) {
        std::vector<std::string> fields = wrenlab::split(lines[i], '\t');
        _platform_table.add(fields);
        std::string probe = fields[0];
        probes.push_back(probe);
        probe_map[probe] = i;
    }
    LOG()->info("{0:d} probes loaded.", probes.size());
    read_next();
}

std::vector<double>*
MiniML::read_sample(std::string accession) {
    if (column_maps.find(accession) == column_maps.end())
        return NULL;
    column_map_t cmap = column_maps[accession];

    if (cmap.find("ID_REF") == cmap.end())
        return NULL;
    if (cmap.find("VALUE") == cmap.end())
        return NULL;

    size_t probe_index = cmap["ID_REF"];
    size_t data_index = cmap["VALUE"];

    std::vector<double>* o = new std::vector<double>(probe_map.size());
    for (size_t i=0; i<probe_map.size(); i++) {
        (*o)[i] = nan("");
    }

    for (std::string line : data_lines()) {
        std::vector<std::string> fields = wrenlab::split(line, '\t');
        if (probe_index > (fields.size() - 1))
            continue;
        if (data_index > (fields.size() - 1))
            continue;
        std::string probe = fields[probe_index];
        double value = atof(fields[data_index].c_str());
        if (probe_map.find(probe) != probe_map.end()) {
            (*o)[probe_map[probe]] = value;
        }
    }
    return o;
}

/* MiniML fetch functions */

std::string
group_prefix(size_t platform_id) {
    std::string s_id = std::to_string(platform_id);
    if (platform_id < 1000) {
        return "GPLnnn";
    } else {
        return "GPL" + s_id.substr(0, floor(log10(platform_id)) - 2) + "nnn";
    }
}

std::string
platform_server_directory(size_t platform_id) {
    return "/geo/platforms/" 
        + group_prefix(platform_id) 
        + "/GPL" 
        + std::to_string(platform_id)
        + "/miniml/";
}


std::vector<std::string>
platform_files(size_t platform_id) {
    std::vector<std::string> o;
    wrenlab::FTPClient client("ftp.ncbi.nlm.nih.gov");
    std::string dir = platform_server_directory(platform_id);
    for (auto e : client.ls(dir)) {
        if (e.attributes["type"] == "file") {
            if (wrenlab::endswith(e.name, ".tgz")) {
                o.push_back(e.path());
            }
        }
    }
    return o;
}

void
miniml_download_platform(size_t platform_id, const char* target) {
    LOG()->info("Downloading MiniML archive for platform GPL{0}", platform_id);
    TemporaryDirectory dldir, dcdir;
    AsperaClient ascp("ftp.ncbi.nlm.nih.gov");
    std::vector<std::string> uris = platform_files(platform_id);
    for (auto uri : uris) {
        ascp.download(uri, dldir.path());
    }
    LOG()->info("Extracting MiniML archives ..."); 

    for (auto file : dldir.ls()) {
      if (file.is_file()) {
        subprocess_call({
            "tar", "-C", dcdir.path(), "-xf", file.path()
        });
      }
    }

    std::string xml_name = "GPL" + std::to_string(platform_id) + "_family.xml";
    std::string ptbl_name = "GPL" + std::to_string(platform_id) + "-tbl-1.txt";

    LOG()->info("Creating reordered archive ..."); 
    TARWriter writer(target, true);
    LOG()->info("Adding XML ...");
    writer.add(dcdir.path() + "/" + xml_name);
    LOG()->info("Adding platform table ...");
    writer.add(dcdir.path() + "/" + ptbl_name);

    std::vector<File> files = dcdir.ls();
    std::sort(files.begin(), files.end());
    ProgressBar pbar(&std::cerr, (size_t) files.size());
    for (size_t i=0; i<files.size(); i++) {
        File file = files[i];
        if (startswith(file.name(), "GSM") & endswith(file.name(), "-tbl-1.txt")) {
            writer.add(file.path());
        }
        pbar.update(i);
    }
    LOG()->info("MiniML archive for GPL{0} created at: {1}", platform_id, target);
}

/*
void miniml_reorder_platform() {
  std::string target("/home/gilesc/tmp/GPL570.miniml.tgz");
  Directory dldir("/home/gilesc/tmp/miniml/");
  Directory dcdir("/home/gilesc/tmp/dc");
  size_t platform_id = 570;

  for (auto file : dldir.ls()) {
    if (file.is_file()) {
      subprocess_call({
          "tar", "-C", dcdir.path(), "-xf", file.path()
          });
    }
  }

  std::string xml_name = "GPL" + std::to_string(platform_id) + "_family.xml";
  std::string ptbl_name = "GPL" + std::to_string(platform_id) + "-tbl-1.txt";

  LOG()->info("Creating reordered archive ..."); 
  TARWriter writer(target, true);
  writer.add(dcdir.path() + "/" + xml_name);
  writer.add(dcdir.path() + "/" + ptbl_name);

  std::vector<File> files = dcdir.ls();
  std::sort(files.begin(), files.end());
  ProgressBar pbar(&std::cerr, (size_t) files.size());
  for (size_t i=0; i<files.size(); i++) {
    File file = files[i];
    if (startswith(file.name(), "GSM") & endswith(file.name(), "-tbl-1.txt")) {
      writer.add(file.path());
    }
    pbar.update(i);
  }
  LOG()->info("MiniML archive for GPL{0} created at: {1}", platform_id, target);
}
*/

};
};
};
