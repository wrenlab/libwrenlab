#include <wrenlab/ncbi/medline.hpp>
#include <wrenlab/log.hpp>

namespace wrenlab {
namespace ncbi {
namespace medline {

void
Parser::parse() {
    doc.parse(input);
    auto xpath = doc.xpath("/PubmedArticleSet/PubmedArticle");
    this->nodes = xpath.nodes();
    ++(*this);
}

bool
Parser::operator++() {
    if (index == nodes.size()) {
        return false;
    }
    wrenlab::XMLNode& node = nodes[index];
    current.id = atoi(node.get_children("PMID")[0].text().c_str());
    current.title = "";
    current.abstract = "";

    auto article_node = node.get_child("Article");
    if (article_node) {
        auto title_node = article_node->get_child("ArticleTitle");
        if (title_node) {
            current.title = title_node->text();
        }
        auto abstract_node = article_node->get_child("Abstract");
        if (abstract_node) {
            auto abstract_text_node = abstract_node->get_child("AbstractText");
            if (abstract_text_node) {
                current.abstract = abstract_text_node->text();
            }
        }
    }
    index++;
    return true;
}

Citation* 
Parser::operator*() {
    return &current;
}

};
};
};
