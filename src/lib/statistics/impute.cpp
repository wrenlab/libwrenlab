#include <utility>
#include <vector>

#include <wrenlab/statistics.hpp>
#include <wrenlab/statistics/impute.hpp>

namespace wrenlab {
namespace statistics {
namespace impute {

// NB: all methods impute by column

arma::vec
nanmean(const arma::mat& X) {
    /* Compute column means */
    arma::vec o(X.n_cols);
    o.fill(arma::datum::nan);
    for (size_t j=0; j<X.n_cols; j++) {
        arma::vec v(X.col(j));
        arma::uvec ix = arma::find_finite(v); 
        size_t n = ix.size();
        if (n > 0) {
            v.replace(arma::datum::nan, 0);
            o(j) = arma::sum(v) / n;
        }
    }
    return o;
}

arma::mat
mean(const arma::mat& X) {
    arma::mat Xi(X);
    arma::vec mu = nanmean(X);
    for (size_t j=0; j<X.n_cols; j++) {
        Xi.col(j).replace(arma::datum::nan, mu(j));
    }
    return Xi.replace(arma::datum::nan, 0);
}

arma::mat
knn(const arma::mat& X, size_t k) {
    throw std::runtime_error("not implemented");
    arma::mat Xi(X);
    auto Xim = wrenlab::statistics::standardize(mean(X));
    auto svd = wrenlab::statistics::randomized_svd(Xim, 5);
    for (size_t j=0; j<X.n_cols; j++) {
        arma::uvec nn = svd.nearest_neighbors(j, k);
        arma::vec mu = arma::mean(X.cols(nn), 1);
        arma::vec c = X.col(j);
        arma::uvec ix = arma::find_nonfinite(c);
        for (size_t jj=0; jj<ix.size(); jj++) {
            Xi(ix(jj),j) = mu(jj);
        }
    }
    return Xi;
}

};
};
};
