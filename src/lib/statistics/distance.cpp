#include <wrenlab/statistics.hpp>
#include <wrenlab/statistics/distance.hpp>
#include <wrenlab/statistics/impute.hpp>

namespace wrenlab {
namespace statistics {
namespace distance {

arma::imat
nearest_neighbors(arma::mat& X, size_t k) {
    arma::imat o(X.n_cols, k);
    arma::mat Xi = wrenlab::statistics::impute::mean(X);
    Xi = wrenlab::statistics::standardize(Xi);
    arma::mat D = Xi.t() * Xi;
    for (size_t i=0; i<X.n_cols; i++) {
        arma::uvec ix = arma::sort_index(D.row(i));
        for (size_t j=1; j<(k+1); j++) {
            o(i,j) = ix(j);
        }
    }
    return o;
}


};
};
};
