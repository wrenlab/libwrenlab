#include <cstdio>
#include <curl/curl.h>
#include <string>

#include <wrenlab/util/fs.hpp>
#include <wrenlab/util/download.hpp>
#include <wrenlab/util/base64.hpp>


namespace wrenlab {

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

bool
curl_download(std::string URL, std::string target) {
    CURL *curl;
    FILE *fp;
    CURLcode rc;
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(target.c_str(), "wb");
        curl_easy_setopt(curl, CURLOPT_URL, URL.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        rc = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }
    return 0;
}

File
download(std::string URL) {
  std::string encoded_url = wrenlab::util::base64::encode(URL);
  Directory cache = get_download_cache_directory();
  File file(cache.path() + "/" + encoded_url);
  if (!file.exists()) {
    curl_download(URL, file.path());
  }
  return file;
}

};
