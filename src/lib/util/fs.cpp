#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>

#include <vector>
#include <iostream>
#include <fstream>

#include <wrenlab/util.hpp>
#include <wrenlab/util/fs.hpp>

namespace wrenlab {

/* Path methods */

File::File(std::string path) {
    while (path[path.size() - 1] == '/') {
        path = path.substr(0, path.size() - 1);
    }
    this->m_path = path;
    refresh();
}

void
File::refresh() {
    stat_rc = stat(m_path.c_str(), &m_stat);
}

bool
File::exists() {
    refresh();
    return stat_rc == 0;
}

bool
File::is_file() {
    return (m_stat.st_mode & S_IFMT) == S_IFREG;
}

bool
File::is_directory() {
    return (m_stat.st_mode & S_IFMT) == S_IFDIR;
}

bool
File::is_symlink() {
    return (m_stat.st_mode & S_IFMT) == S_IFLNK;
}

void
File::remove() {
    unlink(m_path.c_str());
}

std::string
File::name() {
    std::vector<char> s(m_path.begin(), m_path.end());
    s.push_back('\0');
    return std::string(basename(&s[0]));
}

/* Directory functions */

std::vector<File>
Directory::ls() {
    DIR *dir;
    struct dirent *ent;
    std::vector<File> o;

    if ((dir = opendir(m_path.c_str())) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            std::string name(ent->d_name);
            std::string path = m_path + "/" + name;
            if (name == ".")
               continue;
            if (name == "..")
                continue;
            o.push_back(File(path));
        }
        closedir (dir);
    } else {
        throw std::runtime_error("Error listing files in directory: " + m_path);
    }
    return o;
}

Directory
Directory::create(std::string path) {
  int rc = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  return Directory(path);
}

/* TemporaryDirectory functions */

TemporaryDirectory::TemporaryDirectory() {
    char tmpl[1000];
    memset(tmpl, 0, 1000);
    std::string tmpl_ = get_default_temporary_directory() + "/libwrenlab.XXXXXX";
    memcpy(tmpl, tmpl_.c_str(), tmpl_.size());
    this->m_path = mkdtemp(tmpl);
    refresh();
}


TemporaryDirectory::~TemporaryDirectory() {
    std::vector<std::string> args = {
        "rm", "-r", m_path
    };
    subprocess_call(args);
}

/* NamedPipe methods */

/* General functions */

std::string
slurp(std::istream& input) {
    std::string o(
            (std::istreambuf_iterator<char>(input)),
             std::istreambuf_iterator<char>());
    return o;
}

std::string 
slurp (const std::string path) {
    std::ifstream handle(path);
    std::string o = slurp(handle);
    handle.close();
    return o;
}

bool 
file_exists(const std::string& path) {
    struct stat buffer;
    return (stat (path.c_str(), &buffer) == 0); 
}

std::string 
file_path(int fd) {
    char target[10000];
    sprintf(target, "/proc/self/fd/%d", fd);
    char fname[10000];
    readlink(target, fname, 10000);
    return std::string(fname);
}

std::string 
file_path(FILE* file) {
    return file_path(fileno(file));
}

std::string 
make_temporary_file(const std::string prefix) {
    /* 
     * Create a temporary file, returning the path.
     * This file will need to be independently deleted.
     */
    std::string p = prefix + "/libwrenlab.tmp.XXXXXX";
    std::vector<char> pattern (p.begin(), p.end());
    pattern.push_back('\0');
    int tmp = mkstemp(&pattern[0]);
    return file_path(tmp);
}

std::string 
get_default_temporary_directory() {
    const char* o = 
        std::getenv("TMPDIR") != NULL 
        ? std::getenv("TMPDIR") 
        : "/tmp";
    return std::string(o);
}

};
