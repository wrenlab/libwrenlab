#include <wrenlab/util/progress_bar.hpp>

namespace wrenlab {

ProgressBar::ProgressBar(std::ostream* output, size_t maximum) {
    this->output = output;
    this->maximum = maximum;
    this->current = current;
    print();
}

void
ProgressBar::print() {
    (*output) << "[";
    double progress = ((1.0 * current) / maximum);
    int position = width * progress;
    for (int i=0; i<width; ++i) {
        if (i < position)
            (*output) << "=";
        else if (i == position)
            (*output) << ">";
        else
            (*output) << " ";
    }
    (*output) << "] " 
        << int(progress * 100.0) 
        << "% (" << current << "/" << maximum << ")\r" ;
    (*output).flush();
}

void
ProgressBar::update(size_t new_value) {
    this->current = new_value;
    print();
}

void 
ProgressBar::finish() {
    this->current = maximum;
    print();
    (*output) << std::endl;
}

};
