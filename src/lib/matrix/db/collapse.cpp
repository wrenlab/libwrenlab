#include <armadillo>

#include <wrenlab/matrix/db.hpp>
#include <wrenlab/statistics.hpp>
#include <wrenlab/statistics/impute.hpp>

namespace wrenlab {

typedef std::vector<double> (*collapse_fn)(arma::mat&);

arma::vec
reduce_pca(arma::mat& X) {
    auto svd = wrenlab::statistics::randomized_svd(X, (size_t) 1);
    return svd.V.col(0).t();
}

arma::vec
reduce_sum(arma::mat& X) {
    return arma::sum(X, 0).t();
}

arma::vec
reduce_mean(arma::mat& X) {
    return arma::mean(X, 0).t();
}

arma::vec
reduce_max_mean(arma::mat& X) {
    arma::mat Xi = wrenlab::statistics::impute::mean(X);
    arma::vec mu = arma::mean(Xi, 1);
    arma::uvec ix = arma::sort_index(mu, "descend");
    size_t index = ix(0);
    return X.row(index).t();
}

arma::vec
MatrixDB::collapse(
        std::vector<std::string> keys, 
        wrenlab::MatrixDB::AXIS axis, 
        wrenlab::MatrixDB::REDUCTION2D method) {

    arma::mat X;
    if (axis == ROW) {
        X = create_matrix(rows(keys));
    } else {
        X = create_matrix(columns(keys));
    }
    if (X.n_rows == 1) {
        return X.row(0).t();
    }
    arma::vec o;
    switch (method) {
        case SUM:
            o = reduce_sum(X);
            break;
        case MEAN:
            o = reduce_mean(X);
            break;
        case MAX_MEAN:
            o = reduce_max_mean(X);
            break;
        case PCA:
            o = reduce_pca(X);
            break;
        default:
            throw std::runtime_error("Collapse method not recognized.");
    }
    return o;
};

};
