#include <cmath>

#include <wrenlab/matrix/db.hpp>
#include <wrenlab/transpose.hpp>
#include <wrenlab/log.hpp>
#include <wrenlab/compression.hpp>
#include <wrenlab/util/base64.hpp>

namespace wrenlab {

std::string
MatrixDB::get_schema() {
    return std::string(SCHEMA);
}

void
MatrixDB::insert_vector_chunk(
        std::vector<Series>& rows, 
        size_t expected_elements,
        bool is_row) {

    execute("BEGIN TRANSACTION;");

    std::string table = "data";
    if (!is_row) {
        table = "data_t";
    }
    std::string sql = "INSERT INTO " + table + " (key,data) VALUES (?,?);";
    PreparedStatement psr = prepare_statement(sql);
    for (Series& row : rows) {
        assert(row.data.size() == expected_elements);
        std::string encoded_key = wrenlab::util::base64::encode(row.key);
        psr.bind_text(0, encoded_key);
        std::string data_string((const char*) row.data.memptr(), 
                row.data.size() * sizeof(double));
        std::string data_compressed = compress_string(data_string);
        psr.bind_blob(1, (const void*) &data_compressed[0], data_compressed.size());
        psr.step(true);
    }
    execute("END TRANSACTION;");
    psr.commit();
}

size_t 
MatrixDB::insert_vectors(
        SeriesReader& reader, 
        size_t expected_elements,
        bool is_row) {

    std::vector<Series> rows;
    Series* row;

    size_t i = 0;
    size_t BLOCK_SIZE = 100;
    do {
        i += 1;
        rows.push_back(reader.next());
        if (rows.size() == BLOCK_SIZE) {
            insert_vector_chunk(rows, expected_elements, is_row);
            rows.clear();
        }
        if ((i > 0) & (i % BLOCK_SIZE == 0)) {
            LOG()->info("Row block {0}-{1} inserted.", i - BLOCK_SIZE, i);
        }
    } while (!reader.eof());
    if (rows.size() > 0) {
        insert_vector_chunk(rows, expected_elements, is_row);
    }
    return i;
}

void 
MatrixDB::load_indices(bool clear) {
    if (clear) {
        _index.clear();
        _columns.clear();
        r_ix = c_ix = NULL;
    }

    PreparedStatement pr = prepare_statement("SELECT key FROM data ORDER BY i;");
    pr.step();
    while (pr.has_next()) {
        std::string encoded_key = pr.get_text(0);
        std::string decoded_key = wrenlab::util::base64::decode(encoded_key);
        _index.push_back(decoded_key);
        pr.step();
    }
    pr.close();

    PreparedStatement pc = prepare_statement("SELECT key FROM data_t ORDER BY i;");
    pc.step();
    while (pc.has_next()) {
        std::string encoded_key = pc.get_text(0);
        std::string decoded_key = wrenlab::util::base64::decode(encoded_key);
        _columns.push_back(decoded_key);
        pc.step();
    }
    pc.close();

    c_ix = std::make_shared<SeriesIndex>();
    c_ix->initialize(_columns);

    r_ix = std::make_shared<SeriesIndex>();
    r_ix->initialize(_index);
}

void
MatrixDB::initialize(SeriesReader& reader, std::string tmpdir, bool transpose) {
    /*
     * Import the data from a MatrixReader object into this database.
     */
    assert(!read_only);

    std::string schema = get_schema();
    execute(schema);
    //execute("PRAGMA cache_size = 10000;");

    std::vector<std::string> columns = reader.p_index->labels;
    _columns = columns;
    c_ix = reader.p_index;
    size_t ncol = columns.size();
    
    LOG()->info("Inserting row data ...");
    size_t nrow = insert_vectors(reader, ncol, true);

    LOG()->info("{0} rows inserted.", nrow);
    load_indices(false);
    LOG()->info("Indices reloaded.");

    size_t MEMORY_BOUND = 16 * pow(2, 30); // 16 GB
    size_t BLOCK_SIZE = floor(MEMORY_BOUND / (nrow * sizeof(double)));

    LOG()->info("Transposing columns with block size = {}", BLOCK_SIZE);
    ProgressBar pbar(&std::cerr, ncol);
    for (size_t i=0; i<ncol; i += BLOCK_SIZE) {
        pbar.update(i);
        size_t start = i;
        size_t end = (i+BLOCK_SIZE) > ncol ? ncol : i + BLOCK_SIZE;
        //LOG()->debug("Transposing column slice: {0}-{1}", start, end);
        std::vector<std::vector<double> > X;
        std::vector<std::string> local_columns;
        for (std::string ix : _index) {
            Vector* r = this->row(ix);
            if (r == NULL) {
                const char* msg =
                    "Row key not found during data_t initialization: '{0}'";
                LOG()->error(msg, ix);
                exit(1);
            }
            std::vector<double> x(end - start);
            for (int j=0; j<(end - start); j++) {
                x[j] = r->data[start+j];
            }
            X.push_back(x);
            delete r;
        }
        std::vector<std::vector<double> > XT = transpose_vv(X);
        //LOG()->debug("Transposed shape: ({0}, {1})", XT.size(), XT[0].size());
        std::vector<Series> chunk(end - start);
        for (size_t j=0; j<(end - start); j++) {
            arma::vec v(XT[j]);
            chunk[j] = Series(std::string(columns[start+j]), r_ix, v);
        }
        insert_vector_chunk(chunk, _index.size(), false);
    }
    pbar.finish();

    if (transpose) {
        //std::string msg = "object indices may not be valid";
        LOG()->info("Transposition requested; swapping row and column tables.");
        execute("ALTER TABLE data RENAME TO tmp;");
        execute("ALTER TABLE data_t RENAME TO data;");
        execute("ALTER TABLE tmp RENAME TO data_t;");
    }

    LOG()->info("Indexing row keys ...");
    PreparedStatement psr = 
        prepare_statement("CREATE INDEX ix_data_key ON data(key);");
    psr.step(true);
    psr.commit();
 
    LOG()->info("Indexing column keys ...");
    PreparedStatement psc = 
        prepare_statement("CREATE INDEX ix_data_t_key ON data_t(key);");
    psc.step(true);
    psc.commit();

    execute("CREATE INDEX ix_data_i ON data(i);");
    execute("CREATE INDEX ix_data_t_i ON data_t(i);");

    load_indices(true);

    LOG()->info("MatrixDB successfully imported at {0} with shape ({1},{2})",
            path, _index.size(), _columns.size());
}

const std::vector<std::string>&
MatrixDB::columns() {
    return _columns;
}

const std::vector<std::string>&
MatrixDB::index() {
    return _index;
}

void 
MatrixDB::dump(std::ostream& output) {
    // Output columns
    for (std::string c : columns()) {
        output << "\t" << c;
    }
    output << std::endl;

    // Output rows
    for (auto ix : index()) {
        Vector* r = row(ix);
        output << ix;
        for (double x : r->data) {
            output << "\t" << x;
        }
        output << std::endl;
    }
}

Vector*
MatrixDB::row(const std::string key) {
    /* N.B., caller is responsible for deleting the row */
    std::string query = "SELECT key,data FROM data WHERE key=?";
    PreparedStatement ps = prepare_statement(query);
    std::string encoded_key = wrenlab::util::base64::encode(key);
    ps.bind_text(0, encoded_key);
    ps.step();
    if (!ps.has_next()) {
        return NULL;
    }
    Vector* row = new Vector();
    std::vector<char> dc = ps.get_blob(1);
    std::string data_compressed(dc.begin(), dc.end());
    std::string du = decompress_string(data_compressed);
    double* p = (double*) &du[0];
    row->key = key;

    size_t n = du.size() / sizeof(double);
    //std::cerr << n << "\t" << _columns.size() << std::endl;
    assert(n == _columns.size());

    std::vector<double> data;
    data.assign(p, p+n);
    row->data = data;
    return row;
}

Vector*
MatrixDB::column(const std::string key) {
    /* N.B., caller is responsible for deleting the row */
    std::string query = "SELECT key,data FROM data_t WHERE key=?";
    PreparedStatement ps = prepare_statement(query);
    std::string encoded_key = wrenlab::util::base64::encode(key);
    ps.bind_text(0, encoded_key);
    ps.step();
    if (!ps.has_next()) {
        return NULL;
    }
    Vector* row = new Vector();
    std::vector<char> dc = ps.get_blob(1);
    std::string data_compressed(dc.begin(), dc.end());
    std::string du = decompress_string(data_compressed);
    double* p = (double*) &du[0];
    row->key = key;

    size_t n = du.size() / sizeof(double);
    //std::cerr << n << "\t" << _columns.size() << std::endl;
    assert(n == _index.size());

    std::vector<double> data;
    data.assign(p, p+n);
    row->data = data;
    return row;
}

/* Armadillo functions */

arma::vec 
MatrixDB::arow(const std::string key) {
    Vector* v = row(key);
    arma::vec o(v->data);
    delete v;
    return o;
}

arma::vec
MatrixDB::acolumn(const std::string key) {
    Vector* v = column(key);
    arma::vec o(v->data);
    delete v;
    return o;
}

std::string
MatrixDB::get_compressed_data(const std::string& key, bool is_row) {
    std::string table = is_row ? "data" : "data_t";
    std::string query = "SELECT data FROM " + table + " WHERE key=?;";
    PreparedStatement ps = prepare_statement(query);
    std::string encoded_key = wrenlab::util::base64::encode(key);
    ps.bind_text(0, encoded_key);
    ps.step();
    if (!ps.has_next()) {
        throw std::runtime_error("Matrix key error: '" + key + "'");
    }
    std::vector<char> compressed = ps.get_blob(0);
    return std::string(compressed.begin(), compressed.end());
}

arma::mat
MatrixDB::create_matrix(std::vector<std::vector<double> > X) {
    size_t nr = X.size();
    size_t nc = X[0].size();
    arma::mat o(nr, nc);
    for (size_t i=0; i<nr; i++) {
        for (size_t j=0; j<nc; j++) {
            o(i,j) = X[i][j];
        }
    }
    return o;
}

arma::mat
MatrixDB::create_matrix(std::vector<Vector*> X) {
    std::vector<std::vector<double> > data;
    for (size_t i=0; i<X.size(); i++) {
        data.push_back(X[i]->data);
    }
    return create_matrix(data);
}

std::vector<Vector*>
MatrixDB::get_vectors(const std::vector<std::string>& keys, bool is_row) {
    //FIXME: ensure all keys in index
    std::string table = is_row ? "data" : "data_t";
    size_t expected_elements = is_row ? _columns.size() : _index.size();

    std::vector<Vector*> o(keys.size());
    std::vector<std::string> data(keys.size());

    std::string query = "SELECT data FROM " + table + " WHERE key=?;";
    for (size_t i=0; i<keys.size(); i++) {
        data[i] = get_compressed_data(keys[i], is_row);
    }

    #pragma omp parallel for
    for (size_t i=0; i<keys.size(); i++) {
        Vector* v = new Vector();
        std::string du = decompress_string(data[i]);
        double* p = (double*) &du[0];
        size_t n = du.size() / sizeof(double);
        assert(n == expected_elements);
        v->key = keys[i];
        v->data.assign(p, p+n);

        #pragma omp critical
        {
            o[i] = v;
        }
    }

    return o;
}

arma::mat
MatrixDB::slice(
        const std::vector<std::string>& row_ix, 
        const std::vector<std::string>& column_ix) {

    std::vector<std::vector<double> > X;
    std::vector<size_t> selection;
    size_t nr = row_ix.size();
    size_t nc = column_ix.size();
    std::cerr << "nr/nc1:\t" << nr << "\t" << nc << std::endl;

    if (row_ix.size() <= column_ix.size()) {
        for (auto c : column_ix) {
            selection.push_back(c_ix->map[c]);
        }
        std::vector<Vector*> rows = this->rows(row_ix);
        for (size_t i=0; i<nr; i++) {
            Vector* r = rows[i];
            std::vector<double> v(nc);
            for (size_t j=0; j<nc; j++) {
                v[j] = r->data[selection[j]];
            }
            X.push_back(v);
        }
        return create_matrix(X);
    } else {
        for (auto r : row_ix) {
            selection.push_back(r_ix->map[r]);
        }
        std::vector<Vector*> columns = this->columns(column_ix);
        for (size_t j=0; j<nc; j++) {
            Vector* r = columns[j];
            std::vector<double> v(nr);
            for (size_t i=0; i<nr; i++) {
                v[i] = r->data[selection[i]];
            }
            X.push_back(v);
        }
        return create_matrix(X).t();
    }
}

};
