#include <stdio.h>
#include <unistd.h>
#include <cassert>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <sys/resource.h>
#include <thread>
#include <omp.h>
#include <cmath>

#include <wrenlab/util.hpp>
#include <wrenlab/transpose.hpp>

namespace wrenlab {

std::string 
handle(
        std::vector<std::string>& chunk, 
        const std::string& tmpdir, 
        const std::string& compression_program,
        char delimiter) {
    /* 
     * Write chunk to a compressed temporary file,
     * returning the temporary file's path.
     */
    std::vector<std::vector<std::string> > X;
    for (std::string line : chunk) {
        X.push_back(wrenlab::split(line, delimiter));
    }

    std::string path = make_temporary_file(tmpdir);
    std::string cmd = compression_program + " > " + path;
    FILE* h = popen(cmd.c_str(), "w");
   
    std::vector<std::vector<std::string> > Xt = transpose_vv(X);
    for (std::vector<std::string>& v : Xt) {
        fprintf(h, "%s", v[0].c_str());
        for (int i=1; i<v.size(); i++) {
            fputc(delimiter, h);
            fprintf(h, "%s", v[i].c_str());
        }
        fputc('\n', h);
    }
    X.clear();
    pclose(h);
    return path;
}

typedef std::pair<size_t, std::vector<std::string>* >* chunk_t;

chunk_t
get_chunk(std::istream& input, size_t chunk_size, size_t& chunk_index) {
    std::vector<std::string>* chunk = new std::vector<std::string>(chunk_size);
    int i = 0;
    while ((i < chunk_size) && getline(input, (*chunk)[i])) {
        i++;
    }

    if (i == 0) {
        delete chunk;
        return NULL;
    }
    chunk_index++;
    chunk->resize(i);
    return new std::pair<size_t, std::vector<std::string>* >(chunk_index, chunk);
}

size_t get_default_nthreads() {
    return std::max((size_t) (std::thread::hardware_concurrency() - 2), (size_t) 1);
}

void 
transpose_stream(std::istream& input, std::ostream& output, 
        std::string tmpdir, 
        char delimiter) {
    _transpose_stream(input, output, get_default_nthreads(), tmpdir,
            delimiter, "lz4", 100, false);
}

void
_transpose_stream(
        std::istream& input, 
        std::ostream& output,
        size_t n_threads,
        std::string tmpdir,
        char delimiter,
        std::string compression_program,
        size_t chunk_size,
        bool verbose) {

    size_t chunk_index = -1;
    rlimit limit;
    getrlimit(RLIMIT_NOFILE, &limit);
    size_t MAX_OPEN_FILES = limit.rlim_max;

    std::vector<std::string> paths;
    chunk_t p;

    if (n_threads == 0) {
        n_threads = get_default_nthreads();
    }

    omp_set_num_threads(n_threads);

    if (verbose) {
        std::cerr << "Settings:\n";
        std::cerr << "* Temporary directory: " << tmpdir << std::endl;
        std::cerr << "* Chunk size: " << chunk_size << std::endl;
        std::cerr << "* Number of threads: " << n_threads << std::endl;
        std::cerr << std::endl;
    }

    //#pragma omp parallel shared(paths, chunk_index) private (p)
    {
        //#pragma omp master
        while (p = get_chunk(input, chunk_size, chunk_index))
        //#pragma omp task
        {
            size_t ix = p->first;
            std::vector<std::string>* chunk = p->second;
            std::string path = handle(*chunk, tmpdir, compression_program, delimiter);
            //#pragma omp critical
            {
                if (verbose) {
                    std::cerr << "Processing lines " 
                        << ix * chunk_size
                        << "-" << (ix+1)*chunk_size << std::endl;
                }
                if (paths.size() < (ix + 1)) {
                    paths.resize(ix + 1);
                }
                paths[ix] = path;
            }
            delete chunk;
            delete p;
        }
        //#pragma omp taskwait
    }

    std::vector<FILE*> handles;
    for (std::string& path : paths) {
        std::string cmd = "lz4 -dc " + path;
        FILE* h = popen(cmd.c_str(), "r");
        handles.push_back(h);
    }

    bool ok = true;
    while (ok) {
        for (int i=0; i<handles.size(); i++) {
            char* line = NULL;
            size_t n = 0;
            FILE* h = handles[i];

            if (getline(&line, &n, h) == -1) {
                ok = false;
                free(line);
                break;
            } else {
                if (i > 0) {
                    output << delimiter;
                }
                for (int j=n; j--; j>=0) {
                    if (line[j] == '\n') {
                        line[j] = '\0';
                    }
                }
                output << line;
                free(line);
            }
        }
        if (ok)
            output << std::endl;
    }

    for (auto h : handles) {
        pclose(h);
    }

    if (verbose) {
        std::cerr << "\nRemoving temporary files:\n";
    }
    for (std::string& path : paths) {
        if (verbose) {
            std::cerr << "* " << path << std::endl;
        }
        unlink(path.c_str());
    }
}

};
