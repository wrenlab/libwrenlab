#include <wrenlab/util.hpp>
#include <wrenlab/tarfile.hpp>

namespace wrenlab {

/* Public methods */

std::string
TARReader::data() {
    std::string o;
    int size;
    while (true) {
        size = archive_read_data(tar, BUFFER, BUFFER_SIZE);
        if (size < 0) {
            //error
        }
        if (size == 0) {
            break;
        }
        std::string oo(BUFFER, size);
        o += oo;
    }
    return o;
}

TARReader::~TARReader() {
    close();
}


bool 
TARReader::eof() {
    return _eof;
}

void
TARReader::close() {
    if (is_open) {
        rc = archive_read_free(tar);
        this->is_open = false;
    }
}


/* Private methods */

void
TARReader::read_next() {
    rc = archive_read_next_header(tar, &entry);
    _eof = rc == ARCHIVE_EOF;
}

std::vector<std::string> 
TARReader::data_lines() {
    std::string data = this->data();
    auto o = wrenlab::split(data, '\n');
    return std::vector<std::string>(o.begin(), o.begin() + o.size() - 1);
}

std::string
TARReader::current_name() {
  const char* name = archive_entry_pathname(entry);
  if (name == NULL)
    return std::string("");
  else
    return std::string(name);
}

TARReader::TARReader(std::string path) {
    this->path = path;
    this->tar = archive_read_new();
    archive_read_support_filter_all(tar);
    archive_read_support_format_all(tar);
    this->rc = archive_read_open_filename(tar, path.c_str(), block_size);
}


};
