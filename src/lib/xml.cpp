#include <sstream>

#include <wrenlab/util.hpp>
#include <wrenlab/xml.hpp>
#include <wrenlab/log.hpp>

namespace wrenlab {

/* XMLNode methods */

XMLNode::XMLNode(xmlNodePtr p_node) {
    this->node = p_node;
}

std::string
XMLNode::name() {
    return std::string((char*) this->node->name);
}

std::string
XMLNode::text() {
    xmlChar* p_text = xmlNodeGetContent(node);
    std::string o((const char*) p_text);
    xmlFree(p_text);
    return o;
}

std::string
XMLNode::get_attribute(const std::string& key) {
    std::unordered_map<std::string, std::string> attrs = attributes();
    for (auto kv : attrs) {
        if (kv.first == key)
            return kv.second;
    }
    return "";
}

std::unordered_map<std::string, std::string>
XMLNode::attributes() {
    std::unordered_map<std::string, std::string> o;
    xmlChar* p_value;
    for (xmlAttrPtr attr = node->properties; NULL != attr; attr = attr->next) {
        std::string key((char*) attr->name);
        p_value = xmlGetProp(node, attr->name);
        std::string value((char*) p_value);
        o[key] = value;
        xmlFree(p_value);
    }
    return o;
}

std::vector<XMLNode>
XMLNode::children() {
    std::vector<XMLNode> o;
    xmlNodePtr p = node->children;
    for (xmlNodePtr p=node->children; p != NULL; p=p->next) {
        if (p->type == XML_ELEMENT_NODE) {
            o.push_back(p);
        }
    }
    return o;
}

std::vector<XMLNode>
XMLNode::get_children(const std::string& name) {
    // Get first child with the given name
    std::vector<XMLNode> children = this->children();
    std::vector<XMLNode> o;
    for (auto c : children) {
        if (c.name() == name)
            o.push_back(c);
    }
    return o;
}

std::shared_ptr<XMLNode>
XMLNode::get_child(const std::string& name) {
    std::vector<XMLNode> nodes = get_children(name);
    if (nodes.size() >= 1) {
        return std::make_shared<XMLNode>(nodes[0].node);
    } else {
        return nullptr;
    }
}

bool
XMLNode::has_child(const std::string& name) {
    /*
     * Returns true if this node has at least one child with the given name.
     */
    return get_children(name).size() > 0;
}

/* XPath methods */

std::vector<XMLNode>
XPath::nodes() {
    std::vector<XMLNode> o;
    xmlNodeSetPtr nodes = this->obj->nodesetval;
    size_t size = (nodes) ? nodes->nodeNr : 0;
    for (size_t i=0; i<size; i++) {
        o.push_back(XMLNode(nodes->nodeTab[i]));
    }
    return o;
}


/* XMLDocument methods */

XMLDocument::XMLDocument() {
    bool verbose = false;
    if (!verbose) {
        this->options |= XML_PARSE_NOERROR;
        this->options |= XML_PARSE_NOWARNING;
    }
}

XMLDocument::~XMLDocument() {
    // FIXME: see gitlab issue 11
    
    //std::cerr << "begin free doc\n";
    // if (doc != NULL)
    //xmlFreeDoc(doc);
    //std::cerr << "end free doc\n";
}

void
XMLDocument::parse(std::istream& input) {
    const char* URL = NULL;
    std::string xml = slurp(input);
    LOG()->info("Parsing XML document with {0:d} characters", xml.size());
    this->doc = xmlReadMemory(
            xml.c_str(), 
            xml.size(), 
            URL,
            "UTF-8", 
            options);
}

XPath
XMLDocument::xpath(std::string expression) {
    xml_nsmap_t nsmap;
    return xpath(expression, nsmap);
}

XPath
XMLDocument::xpath(std::string expression, xml_nsmap_t nsmap) {
    xmlXPathContextPtr ctx = xmlXPathNewContext(doc);
    assert (ctx != NULL);
    for (auto pair : nsmap) {
        xmlXPathRegisterNs(ctx, 
                (const unsigned char*) pair.first.c_str(), 
                (const unsigned char*) pair.second.c_str());
    }
    xmlXPathObjectPtr obj = xmlXPathEvalExpression(
            (const unsigned char*) expression.c_str(), ctx);
    return XPath(ctx, obj);
}

};
