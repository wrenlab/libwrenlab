#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <limits>
#include <fstream>
#include <iostream>
#include <memory>
#include <sys/sysinfo.h>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <cstdlib>

#include <iostream>

#include <wrenlab/util.hpp>
#include <wrenlab/log.hpp>

using namespace std;

namespace wrenlab {

bool
startswith(const std::string s, const std::string q) {
    for (size_t i=0; i<q.size(); i++) {
        if (s[i] != q[i]) {
            return false;
        }
    }
    return true;
}

bool
endswith(const std::string s, const std::string q) {
    std::string sr(s.rbegin(), s.rend());
    std::string qr(q.rbegin(), q.rend());
    return startswith(sr, qr);
}

std::string 
lowercase(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

std::string 
uppercase(string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
    return s;
}

std::string
join(const std::string sep, const std::vector<std::string> elements) {
    std::string o;
    if (elements.size() == 0)
        return o;
    for (int i=0; i<elements.size() - 1; i++) {
        o += elements[i];
        o += sep;
    }
    o += elements[elements.size() - 1];
    return o;
}

std::vector<std::string> 
split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    int start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

/*
std::string
lstrip(const std::string& text) {
  for (size_t i=0; i<text.size(); i++) {
    char c = text[i];
    if ((c != ' ') && (c != '\n') && (c != '\t')) {
      return text.substr(i);
    }
  }
  return "";
}
*/

std::string
strip (std::string text) {
  size_t start = text.find_first_not_of(" \n\t");
  if (start == std::string::npos) {
    start = 0;
  }
  size_t end = text.find_last_not_of(" \n\t");
  if (end == std::string::npos) {
    end = text.size();
  }
  return text.substr(start, end + 1 - start);
}

std::string 
exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != NULL)
            result += buffer.data();
    }
    return result;
}

void
subprocess_call(std::vector<std::string> args) {
    std::string cmd = join(" ", args);
    LOG()->debug("Calling subprocess with command: '{0}'", cmd);
    system(cmd.c_str());
}

void 
replace_all(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        // In case 'to' contains 'from', like replacing 'x' with 'yx'
        start_pos += to.length(); 
    }
}

unsigned long available_memory() {
  std::string token;
  std::ifstream file("/proc/meminfo");
  while(file >> token) {
    if(token == "MemTotal:") {
      unsigned long mem;
      if(file >> mem) {
        return mem;
      } else {
        return 0;       
      }
    }
    // ignore rest of the line
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  return 0; // nothing found
}


};
