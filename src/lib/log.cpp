#include <wrenlab/log.hpp>

namespace wrenlab {

std::shared_ptr<spdlog::logger> _LOG = NULL;

std::shared_ptr<spdlog::logger>
LOG() {
    if (_LOG == NULL)
        _LOG = spdlog::stderr_color_mt("libwrenlab");
    return _LOG;
}

};
