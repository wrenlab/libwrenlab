#include <wrenlab/statistics.hpp>

namespace wrenlab {
namespace statistics {

arma::mat
standardize(const arma::mat& X) {
    /* Standardize a matrix by columns */
    arma::mat o(X);
    for (size_t j=0; j<X.n_cols; j++) {
        arma::colvec c = X.col(j);
        o.col(j) = (c - arma::mean(c)) / arma::stddev(c);
    }
    return o;
}

SVDResult
randomized_svd(arma::mat& X, size_t rank) {
    /* Randomized SVD according to:
     * https://stats.stackexchange.com/questions/2806/best-pca-algorithm-for-huge-number-of-features-10k
     */

    size_t k = rank;

    bool is_transposed = false;
    if (X.n_rows < X.n_cols) {
        X = X.t();
        is_transposed = true;
    }

    size_t nc = X.n_cols;
    arma::mat G = arma::randn<arma::mat>(nc, k + 2);
    arma::mat H = X * G;
    if (true) {
        // "power method"
        for (size_t j=1; j<k + 1; j++) {
            H = X * (X.t() * H);
        }
    }

    arma::mat Q,R;
    arma::qr_econ(Q,R,H);
    arma::mat T = X.t() * Q;

    arma::mat Vt,W;
    arma::vec st;
    arma::svd_econ(Vt,st,W,T);

    arma::mat Ut = Q * W;

    arma::mat U,V;
    if (is_transposed) {
        V = Ut.cols(0, k-1);
        U = Vt.cols(0, k-1);
    } else {
        U = Ut.cols(0, k-1);
        V = Vt.cols(0, k-1);
    }
    //arma::mat S = st.subvec(0, k-1) * arma::eye(k,k) 
    arma::mat S = arma::diagmat(st.subvec(0, k-1));

    SVDResult o;
    o.U = U;
    o.V = V;
    o.S = S;
    return o;
}

SVDResult 
randomized_svd(arma::mat& X) {
    return randomized_svd(X, 3);
}

};
};
