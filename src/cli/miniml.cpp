#include <iostream>

#include <wrenlab/ncbi/geo/miniml.hpp>

void usage(int rc=0) {
  std::cerr << "miniml <subcommand> <args>\n";
}

int dump(int argc, char* argv[]) {
  std::string path(argv[2]);
  wrenlab::ncbi::geo::MiniML miniml(path);
  miniml.dump(std::cout);
  return 0;
}

int ptbl(int argc, char* argv[]) {
  std::string path(argv[2]);
  wrenlab::ncbi::geo::MiniML miniml(path);
  wrenlab::Table ptbl = miniml.platform_table();
  ptbl.dump(std::cout, '\t');
  return 0;
}

int cmap (int argc, char* argv[]) {
  std::string path(argv[2]);
  wrenlab::ncbi::geo::MiniML miniml(path);
  miniml.print_column_map();
  return 0;
}

int fetch(int argc, char* argv[]) {
  size_t platform_id = atol(argv[2]);
  if (platform_id == 0) {
    std::string msg = "Error parsing platform ID from string: '" 
      + std::string(argv[2]) + "'";
    throw std::runtime_error(msg);
  }
  std::string target;
  if (argc >= 4) {
    target = std::string(argv[3]);
  } else {
    target = "GPL" + std::to_string(platform_id) + ".tgz";
  }
  wrenlab::ncbi::geo::miniml_download_platform(platform_id, target.c_str());
  return 0;
}

int main(int argc, char* argv[]) {
  std::string cmd(argv[1]);
  if (cmd == "dump") {
    return dump(argc, argv);
  } else if (cmd == "ptbl") {
    return ptbl(argc, argv);
  } else if (cmd == "cmap") {
    return cmap(argc, argv);
  } else if (cmd == "fetch") {
    return fetch(argc, argv);
  } else {
    throw std::runtime_error("invalid command");
  }
}
