#include <set>
#include <string>

#include <armadillo>

#include <wrenlab/matrix/db.hpp>
#include <wrenlab/statistics.hpp>
#include <wrenlab/statistics/impute.hpp>
#include <wrenlab/util/base64.hpp>
#include <wrenlab/util.hpp>
#include <wrenlab/ncbi/geo/miniml.hpp>

/*
int main() {
    wrenlab::MatrixDB XDB("/data/matrixdb/ncbi/geo/probe/GPL96.matrixdb");
    arma::vec o = XDB.reduce(&wrenlab::statistics::sum);
    std::cout << o << std::endl;
}
*/

int main7() {
  wrenlab::ncbi::geo::miniml_reorder_platform();
}

int main6() {
    wrenlab::SeriesReader rdr("/dev/stdin");
    for (auto c : rdr.p_index->labels) {
        std::cout << c << std::endl;
    }
}

int main5() {
    std::string s = "hello world";
    std::string encoded = wrenlab::util::base64::encode(s);
    std::string decoded = wrenlab::util::base64::decode(encoded);
    std::cout << "'" << encoded << "'" << "\t'" << decoded << "'" << std::endl;
}

int main4() {
    arma::mat X = arma::randn(10,5);
    X(1,1) = arma::datum::nan;
    X(4,2) = arma::datum::nan;
    std::cout << wrenlab::statistics::impute::knn(X,3) << std::endl;
}

int main2() {
    wrenlab::MatrixDB XDB("/data/matrixdb/ncbi/geo/probe/GPL96.matrixdb");
    auto ix = std::vector<std::string>(XDB.index().begin(), XDB.index().end());
    auto cx = std::vector<std::string>(XDB.columns().begin(), XDB.columns().begin() + 50);
    arma::mat X = XDB.slice(ix, cx);
    arma::mat Xi = wrenlab::statistics::impute::knn(X,5);
    std::cerr << Xi << std::endl;
}

int main3(int argc, char* argv[]) {
    //wrenlab::MatrixDB X("/data/matrixdb/ebi/metagenome.matrixdb");
    wrenlab::MatrixDB X("/data/matrixdb/ncbi/geo/probe/GPL96.matrixdb");
    std::vector<std::string> ix = std::vector<std::string>(X.columns().begin(), X.columns().begin() + 5);
    auto o = X.collapse(ix, wrenlab::MatrixDB::AXIS::COLUMN, wrenlab::MatrixDB::REDUCTION2D::PCA);
    for (auto v : o) {
        std::cout << v << std::endl;
    }
}
