#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <cassert>
#include <stdio.h>
#include <unistd.h>

#include <wrenlab.hpp>

using namespace std;
using namespace wrenlab::AhoCorasick;

/*
void test() {
	string s = "he likes his caffeine she hers";
	vector<Match> result = t.search(s);
	for (int i=0; i<result.size(); i++) {
		cout << result[i].id << "\t" << result[i].start 
            << "\t" << result[i].end << endl;
	}
	return 0;
}
*/

set<string> get_stopwords() {
    const char* cmd = 
        "aspell -d en_US.multi dump master | aspell -l en -d en_US.multi expand";
    FILE* h = popen(cmd, "r");
    assert(h != NULL);
    set<string> words;
    size_t n = 0;
    char* line = NULL;
    while ((getline(&line, &n, h)) != -1) {
        string s(line, n);
        s.erase(s.find_last_of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-'")+1);
        if (!s.empty())
            words.insert(s);
        free(line);
        line = NULL;
    }
    int rc = pclose(h);
    return words;
}

void usage() {
    cerr << 
        "USAGE: tmatch [-i path] [-s path] < input\n"
        "Find multiple terms in input text.\n\n"
        "Arguments:\n"
        "   -i <path> : Path to file containing terms to be searched case insensitively\n"
        "   -s <path> : Path to file containing terms to be searched case sensitively\n"
        "   -h : Show this help\n\n"
        "Input formats:\n"
        "   stdin : The text to be searched. Must have at least two columns, tab delimited.\n"
        "           The first is the record key, and the remaining columns contain text.\n"
        "   term files : Two columns, tab delimited.\n"
        "                Column 1: integer term ID\n"
        "                Column 2: text of the term\n";
    exit(1);
}

const int MIN_SYNONYM_LENGTH = 5;

struct Term {
  std::string key;
  std::string text;
};

std::vector<Term>
read_terms(const std::string& path) {
  std::ifstream handle(path);
  std::string line;
  std::vector<Term> o;

  while (getline(handle, line)) {
    std::vector<std::string> fields = wrenlab::split(line);
    std::string key = fields[0];
    std::string text = fields[1];
    Term term = {key, text};
    o.push_back(term);
  }

  return o;
}

void populate_trie(Trie& t, const string path, const set<string>& stopwords) {
	ifstream file(path);
	string line;
	while (getline(file, line)) {
        vector<string> fields = wrenlab::split(line);
        string id = fields[0];
        t.add(id, fields[1]);
        //int id = atoi(fields[0].c_str());
        if (fields[1].size() < MIN_SYNONYM_LENGTH) {
            continue;
        }
        if (stopwords.find(fields[1]) != stopwords.end()) {
            continue;
        }
        if (stopwords.find(wrenlab::lowercase(fields[1])) != stopwords.end()) {
            continue;
        }
	}
	t.build();
}

int main(int argc, char* argv[]) {
    string cs_path = "", ci_path = "";
    bool compact = false;
    int c;
    while ((c = getopt(argc, argv, "chi:s:")) != -1) {
      switch (c) {
        case 'i':
          ci_path = string(optarg);
          break;
        case 's':
          cs_path = string(optarg);
          break;
        case 'h':
          usage();
          break;
        case 'c':
          compact = true;
          break;
      }
    }

    if (cs_path.empty() && ci_path.empty())
        usage();

    //set<string> stopwords = get_stopwords();
    set<string> stopwords;

    std::string line;
    MixedCaseSensitivityTrie trie(true, " ");

    if (!cs_path.empty()) {
      for (Term term : read_terms(cs_path)) {
        trie.add(term.key, term.text, true);
      }
    }
    if (!ci_path.empty()) {
      for (Term term : read_terms(ci_path)) {
        trie.add(term.key, term.text, false);
      }
    }

    std::cerr << "Building trie..." << std::endl;
    trie.build();
    std::cerr << "Built trie!" << std::endl;

    while (getline(cin, line)) {
        vector<string> fields = wrenlab::split(line);
        string key = fields[0];
        string text = "";
        for (int i=1; i<fields.size(); i++) {
            if (i > 1)
                text.append(" ");
            text.append(fields[i]);
        }
        std::vector<Match> matches = trie.search(text);

        std::set<std::string> hits;
        for (Match m : matches) {
            hits.insert(m.id);
        }
        if (compact) {
          if (hits.size() < 2)
            continue;
          cout << key;
          for (const std::string& hit : hits) {
            cout << "\t" << hit;
          }
          cout << endl;
        } else {
          for (const std::string& hit : hits) {
              cout << key << "\t" << hit << endl;
          }
        }
    }
}
