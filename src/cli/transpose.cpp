#include <stdio.h>
#include <unistd.h>
#include <cassert>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <sys/resource.h>
#include <thread>
#include <omp.h>
#include <cmath>

#include <wrenlab.hpp>

/*
 * TODO: 
 * - Use TMPDIR (or TEMPDIR?) environment variable before /tmp if NOS 
 * - It is not actually necessary to transpose the data ever, just
 *      write it out looping j over i
 * - Leaking memory? (Seems to occur if: lots of processors running with big
 *   chunk size and they get I/O blocked. Hopefully addition of compression 
 *   helped this).
 */

using namespace std;

void print_usage() {
    cerr 
        << "Transpose a delimited file on STDIN, and output the results to STDOUT.\n"
        << "Can be used for very large inputs.\n\n"
        << "USAGE: transpose [options] < input.matrix > transposed.matrix\n\n"
        << "Options:\n"
        << " -t <tmpdir>    : Specify an alternate temporary directory\n"
        << " -n <chunksize> : Specify the number of lines to be read at once\n"
        << "                  (affects memory usage)\n"
        << " -p <n>         : Specify the number of threads\n"
        << " -c <program>   : Specify the external compression program to use.\n"
        << "                  By default, this is lz4, if the system has it,\n"
        << "                  otherwise gzip. (The binary must be on your PATH).\n"
        << " -d <delimiter> : Specify the matrix delimiter (default: tab).\n"
        << "                  Must be a single character.\n"
        << " -q             : Run quietly/non-verbosely.\n"
        << " -h             : Display this help message.\n\n"
        << "NOTES:\n"
        << "* The input matrix is assumed to be square (same number of columns per row)\n"
        << "* Memory usage scales with the parameters -n and -p. Should not be a\n"
        << "    problem except for very slow I/O situations.\n";

}

int 
main(int argc, char** argv) {
    size_t chunk_size = 100;
    char delimiter = '\t';
    // TODO: check existence
    string compression_program = "lz4";
    size_t n_threads = 0;
    bool verbose = true;

    string tmpdir = "/tmp";
    if (getenv("TMPDIR") != NULL) {
        tmpdir = getenv("TMPDIR");
    }

    // Parse CLI
    char c;
    while ((c = getopt(argc, argv, "qd:t:c:p:n:h")) != -1) {
        switch (c) {
            case 'q':
                verbose = false;
                break;
            case 't':
                tmpdir = optarg;
                break;
            case 'n':
                chunk_size = atoi(optarg);
                break;
            case 'p':
                n_threads = atoi(optarg);
                break;
            case 'h':
                print_usage();
                exit(0);
            case 'c':
                compression_program = optarg;
                break;
            case 'd':
                if (optarg[1] != '\0') {
                    cerr << "ERROR: delimiter (-d) must be a single character.\n";
                    exit(1);
                }
                delimiter = optarg[0];
                break;
            default:
                print_usage();
                exit(1);
        }
    }
    wrenlab::_transpose_stream(cin, cout, n_threads, 
            tmpdir, delimiter, compression_program,
            chunk_size, verbose);
}
