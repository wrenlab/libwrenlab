#include <wrenlab/ncbi/geo/miniml.hpp>

void
print_usage(int rc=1) {
    std::cerr << "USAGE: fetch-miniml <accession> <target>\n";
    exit(rc);
}

int
main (int argc, char* argv[]) {
    if (argc == 1)
        print_usage();

    size_t platform_id = atol(argv[1]);
    if (platform_id == 0) {
        std::string msg = "Error parsing platform ID from string: '" 
            + std::string(argv[1]) + "'";
        throw std::runtime_error(msg);
    }

    std::string target;
    if (argc >= 3) {
        target = std::string(argv[2]);
    } else {
        target = "GPL" + std::to_string(platform_id) + ".tgz";
    }
    wrenlab::ncbi::geo::miniml_download_platform(platform_id, target.c_str());
}
