#include <wrenlab/ncbi/medline.hpp>
#include <wrenlab/compression/gzstream.hpp>

int main(int argc, char* argv[]) {
    std::string path = "/data/ncbi/MEDLINE/current/pubmed18n0161.xml.gz";
    wrenlab::ncbi::medline::Parser parser(path);
    parser.parse();
    wrenlab::ncbi::medline::Citation* p_citation;
    while (++parser) {
      p_citation = *parser;
      std::cout << p_citation->id << std::endl;
      std::cout << p_citation->title << std::endl;
      std::cout << p_citation->abstract << "\n\n";
    }
}
