#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void ac_trie;
typedef void ac_match;

ac_trie* ac_new();
void ac_delete(ac_trie*);
void ac_add(char*, void*);
void ac_build(ac_trie*);

ac_match* ac_search(ac_trie*, const char*);
void ac_match_delete(ac_match*);
size_t ac_match_size(ac_match*);
void* ac_match_key(ac_match*, size_t);

#ifdef __cplusplus
}
#endif
