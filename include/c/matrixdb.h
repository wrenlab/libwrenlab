#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void matrixdb;
typedef void matrixdb_vector;
typedef const void matrixdb_index;

/* DB functions */

matrixdb* matrixdb_open(const char* path, bool read_only=true);
void matrixdb_close(matrixdb*);

/* Vector functions */

matrixdb_vector* matrixdb_get_column(matrixdb*, const char* key);
matrixdb_vector* matrixdb_get_row(matrixdb*, const char* key);
const char* matrixdb_vector_key(matrixdb_vector*);
size_t matrixdb_vector_size(matrixdb_vector*);
double* matrixdb_vector_data(matrixdb_vector*);
void matrixdb_vector_delete(matrixdb_vector*);

/* Index functions */

matrixdb_index* matrixdb_get_row_index(matrixdb*);
matrixdb_index* matrixdb_get_column_index(matrixdb*);
size_t matrixdb_index_size(matrixdb_index* ix);
void matrixdb_index_element(matrixdb_index* ix, size_t i, char* result);

/* Operations */

matrixdb_vector* matrixdb_collapse(matrixdb* db, 
        char** keys, 
        size_t n, 
        size_t,
        const char*);

#ifdef __cplusplus
}
#endif
