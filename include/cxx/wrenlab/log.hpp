#pragma once

#include <memory>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace wrenlab {

std::shared_ptr<spdlog::logger> LOG();

};
