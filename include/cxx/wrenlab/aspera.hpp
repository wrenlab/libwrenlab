#pragma once

#include <cstdlib>
#include <string>

#include <wrenlab/util.hpp>
#include <wrenlab/log.hpp>

#include <iostream>

namespace wrenlab {

class AsperaError {
public:
    AsperaError() {};
};

class AsperaClient {
private:
    std::string host, user;
    std::string binary_path, key_path;
    size_t transfer_limit_megabyte = 200;

public:
    AsperaClient(std::string host, std::string user="anonftp") {
        this->user = user;
        this->host = host;
        std::string home(std::getenv("HOME"));
        this->binary_path = home + + "/.aspera/connect/bin/ascp";
        this->key_path = home + + "/.aspera/connect/etc/asperaweb_id_dsa.openssh";

        if (!(file_exists(binary_path) & file_exists(key_path))) {
            throw AsperaError();
        }
    }

    void
    download(std::string source, std::string destination) {
        LOG()->info("[aspera] Downloading {0} ...", source);
        std::string source_uri =
            user + "@" + host + ":" + source;
        std::vector<std::string> args = {
            binary_path,
            "-T",
            "-i", key_path,
            "--apply-local-docroot",
            "-l", std::to_string(transfer_limit_megabyte) + "M",
            source_uri, 
            destination
        };
        std::string cmd = wrenlab::join(" ", args);
        wrenlab::exec(cmd.c_str());
    }
};

};
