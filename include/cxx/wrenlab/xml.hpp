/*
 * NOTES: do something with xmlNodePtr->type
 */

#pragma once

#include <string>
#include <cassert>
#include <unordered_map>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <vector>
#include <memory>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

namespace wrenlab {

typedef std::unordered_map<std::string, std::string> xml_nsmap_t;

class XMLNode {
private:
    xmlNodePtr node;

public:
    XMLNode(xmlNodePtr);
    /*
    XMLNode(xmlNodePtr node) {
        this->node = node;
    }
    */

    std::string name();
    std::string text();

    /* Return value if found, otherwise empty string */
    std::string get_attribute(const std::string&);

    /* Return map of node attributes */
    std::unordered_map<std::string, std::string> attributes();

    /* Return vector of all children (of type XML_ELEMENT_NODE) */
    std::vector<XMLNode> children();

    /* Get all child nodes with the given name */
    std::vector<XMLNode> get_children(const std::string&);
    /* Get first child node with the given name */
    std::shared_ptr<XMLNode> get_child(const std::string&);

    /* Check if this node has at least one child with the given name. */
    bool has_child(const std::string&);
};

class XPath {
private:
    xmlXPathContextPtr ctx;
    xmlXPathObjectPtr obj;

public:
    XPath(xmlXPathContextPtr ctx, xmlXPathObjectPtr obj) {
        this->ctx = ctx;
        this->obj = obj;
    }

    ~XPath() {
        xmlXPathFreeObject(obj);
        xmlXPathFreeContext(ctx);
    }

    std::vector<XMLNode> nodes();
};

class XMLDocument {
private:
    xmlDocPtr doc;
    int options = XML_PARSE_HUGE | XML_PARSE_NONET | XML_PARSE_RECOVER;

public:
    XMLDocument();
    ~XMLDocument();

    void parse(std::istream&);
    XPath xpath(std::string expression);
    XPath xpath(std::string expression, xml_nsmap_t nsmap);
};

};
