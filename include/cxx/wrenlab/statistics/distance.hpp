#pragma once

#include <armadillo>

namespace wrenlab {
namespace statistics {
namespace distance {

typedef double (*distance_fn)(double*, double*, size_t);

arma::imat
nearest_neighbors(arma::mat&, size_t);

};
};
};
