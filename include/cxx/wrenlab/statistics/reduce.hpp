#pragma once

#include <armadillo>

namespace wrenlab {
namespace statistics {

typedef double (*reduction1d_fn)(const arma::Col<double>&);
typedef arma::vec (*reduction2d_fn)(const arma::mat&);

//double sum(arma::Col<double> const&);

/*
double 
sum(arma::Col<double> const& v) {
    return arma::sum(v);
}
*/

};
};
