#pragma once

#include <armadillo>

namespace wrenlab {
namespace statistics {
namespace impute {

enum Strategy {
    MEAN = 0,
    KNN = 1
};

arma::mat mean(const arma::mat&);
arma::mat knn(const arma::mat&, size_t);

};
};
};
