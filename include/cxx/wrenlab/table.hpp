#include <string>
#include <vector>
#include <iostream>

namespace wrenlab {

class Table {
private:
  std::vector<std::string> _columns;
  std::unordered_map<std::string, size_t> _column_map;
  std::vector<std::vector<std::string> > _rows;

public:
  Table() {}

  Table(std::vector<std::string>& columns) {
    set_columns(columns);
  }

  void set_columns(std::vector<std::string>& columns) {
    assert (_rows.size() == 0);
    this->_columns = columns;
    for (size_t j=0; j<_columns.size(); j++) {
      _column_map[_columns[j]] = j;
    }
  }

  void add(std::vector<std::string> row) {
    assert(row.size() == _columns.size());
    _rows.push_back(row);
  }

  size_t nrow() {
    return _rows.size();
  }

  size_t ncol() {
    return _columns.size();
  }

  std::vector<std::string> 
  row(size_t i) {
    return _rows[i];
  }

  std::vector<std::string> 
  column(size_t j) {
    std::vector<std::string> o;
    for (size_t i=0; i<nrow(); i++) {
      o.push_back(_rows[i][j]);
    }
    return o;
  }

  std::vector<std::string>
  column(std::string key) {
    size_t j = _column_map[key];
    return column(j);
  }

  void
  dump(std::ostream& output, char sep='\t') {
    output << _columns[0];
    for (size_t j=1; j<_columns.size(); j++) {
      output << sep;
    }
    output << std::endl;

    for (auto& row : _rows) {
      output << row[0];
      for (size_t j=1; j<_columns.size(); j++) {
        output << sep << row[j];
      }
      output << std::endl;
    }
  }
};

};
