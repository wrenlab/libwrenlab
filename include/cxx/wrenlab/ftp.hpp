#pragma once

#include <curl/curl.h>

#include <wrenlab/log.hpp>

namespace wrenlab {

struct FTPDirectoryEntry {
    std::string directory;
    std::string name;
    std::unordered_map<std::string, std::string> attributes;

    std::string path() {
        return directory + name;
    }
};

class FTPClient {
private:
    CURL* curl;
    std::string host;

public:
    FTPClient(std::string host) {
        curl = curl_easy_init();
        this->host = host;
        //curl_global_init(CURL_GLOBAL_ALL);
    }

    ~FTPClient() {
        curl_easy_cleanup(curl);
        //curl_global_cleanup();
    }

    std::vector<FTPDirectoryEntry> ls(std::string subdir);
};

class FTPError {
private:
    friend FTPClient FTPError() {};
};

};
