#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <memory>

#include <wrenlab/util.hpp>
#include <wrenlab/util/fs.hpp>
#include <wrenlab/util/download.hpp>
#include <wrenlab/tarfile.hpp>
#include <wrenlab/util/cache.hpp>
#include <wrenlab/sqlite.hpp>

namespace wrenlab {
namespace ncbi {
namespace taxonomy {

const char* SCHEMA = R"EOF(
CREATE TABLE taxon (
  id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR
);
)EOF";

struct Taxon {
public:
  size_t id;
  std::string name;
  Taxon(size_t id, std::string name) : id(id), name(name) {};
};

class Taxonomy {
private:
  SQLiteDB* cx;

  void
  initialize(std::string db_path) {
    wrenlab::SQLiteDB cx(db_path, false);
    cx.execute(SCHEMA);

    std::string URL = "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz";
    wrenlab::File file = wrenlab::download(URL);
    wrenlab::TARReader archive(file.path());
    do {
      archive.read_next();
      if (archive.current_name() == "names.dmp")
        parse_names(cx, archive.data());
    } while (!archive.eof());
    cx.close();
  }

  void 
  parse_names(wrenlab::SQLiteDB& cx, const std::string& data) {
    wrenlab::PreparedStatement ps = cx.prepare_statement("INSERT INTO taxon VALUES (?,?);");

    std::vector<std::string> lines = wrenlab::split(data, '\n');
    size_t n =0;
    for (std::string& line : lines) {
      n++;
      if ((n > 0) & (n % 1000 == 0))
        std::cerr << "Loading taxonomy line: " << n << std::endl;
      line = wrenlab::strip(line);
      std::vector<std::string> fields = wrenlab::split(line, '|');
      for (size_t i=0; i<fields.size(); i++) {
        fields[i] = wrenlab::strip(fields[i]);
      }
      if (fields.size() != 5)
        continue;
      if (fields[3] != "scientific name")
        continue;
      size_t taxon_id = atoi(fields[0].c_str());
      std::string name = fields[1];
      ps.bind_integer(0, taxon_id);
      ps.bind_text(1, name);
      ps.step(true);
    }
    ps.commit();
    ps.close();
  }

  std::shared_ptr<Taxon>
  fetch_single(wrenlab::PreparedStatement& ps) {
    while (ps.has_next()) {
      size_t id = ps.get_integer(0);
      std::string name = ps.get_text(1);
      return std::make_shared<Taxon>(id, name);
    }
    return NULL;
  }

public:
  Taxonomy() {
    File file(wrenlab::get_cache_directory().path() + "/taxonomy.db");
    if (!file.exists())
      initialize(file.path());
    cx = new wrenlab::SQLiteDB(file.path());
  }

  std::shared_ptr<Taxon>
  by_id(size_t taxon_id) {
    wrenlab::PreparedStatement ps = cx->prepare_statement("SELECT * FROM taxon WHERE id=?;");
    ps.bind_integer(0, taxon_id);
    return fetch_single(ps);
  }

  std::shared_ptr<Taxon>
  by_name(const std::string& name) {
    wrenlab::PreparedStatement ps = cx->prepare_statement("SELECT * FROM taxon WHERE name=?;");
    ps.bind_text(0, name);
    return fetch_single(ps);
  }
};

};
};
};
