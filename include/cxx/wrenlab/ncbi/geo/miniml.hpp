#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <cmath>

#include <wrenlab/util.hpp>
#include <wrenlab/tarfile.hpp>
#include <wrenlab/xml.hpp>
#include <wrenlab/table.hpp>

namespace wrenlab {
namespace ncbi {
namespace geo {

typedef std::unordered_map<std::string, size_t> column_map_t;

void
miniml_download_platform(size_t, const char*);

std::unordered_map<std::string, column_map_t>
    parse_sample_column_map(std::string xml);

class MiniML : public TARReader {
private:
    std::string accession;
    std::vector<std::string> probes;
    wrenlab::Table _platform_table;
    std::unordered_map<std::string, size_t> probe_map;
    std::unordered_map<std::string, column_map_t> column_maps;

    void initialize();

public:
    std::vector<double>*
    read_sample(std::string accession);

    MiniML(const std::string path) : TARReader(path) {
        initialize();
    }

    void print_column_map(std::ostream& output=std::cout) {
        for (auto kv : column_maps) {
            for (auto kv2 : kv.second) {
                output << kv.first
                    << "\t" << kv2.first 
                    << "\t" << kv2.second 
                    << std::endl;
            }
        }
    }

    wrenlab::Table
    platform_table() {
      return _platform_table;
    }

    void dump(std::ostream& output) {
        std::string name, sample_accession;
        std::vector<double> v;

        for (auto p : probes) {
            output << "\t" << p;
        }
        output << "\n";

        do {
            read_next();
            if (eof())
              break;
            name = current_name();
            if (name.empty())
              continue;
            if (name.substr(0, 3) != "GSM") {
              continue;
            }
            sample_accession = wrenlab::split(name, '-')[0];
            std::vector<std::string> o;
            try {
              std::vector<double>* sample_data = read_sample(sample_accession);
              if (sample_data == NULL)
                  continue;
              o.push_back(sample_accession);
              for (double x : *sample_data) {
                o.push_back(std::to_string(x));
              }
              /*
              output << sample_accession;
              for (double x : *sample_data) {
                  output << "\t" << x;
              }
              output << "\n";
              */
              delete sample_data;
            } catch (std::exception e) {
              LOG()->error("Error processing:", sample_accession);
              continue;
            } 
            std::string line = wrenlab::join("\t", o);
            output << line << "\n";
        } while (!eof());
    }
};

//void miniml_reorder_platform();

};
};
};
