#pragma once

#include <string>
#include <vector>

#include <wrenlab/util/iterator.hpp>
#include <wrenlab/xml.hpp>
#include <wrenlab/compression/gzstream.hpp>

namespace wrenlab {
namespace ncbi {
namespace medline {

struct Citation {
    size_t id;
    std::string title, abstract;
};

class Parser {
private:
    wrenlab::igzstream input;
    wrenlab::XMLDocument doc;
    std::vector<wrenlab::XMLNode> nodes;
    size_t index = 0;
    Citation current;

public:
    Parser(const std::string& path) : input(path.c_str()) {}
    void parse();

    bool operator++();
    Citation* operator*();
};

};
};
};
