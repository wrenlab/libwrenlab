#pragma once

#include <vector>

namespace wrenlab {

template <typename T>
std::vector<std::vector<T> >
transpose_vv(std::vector<std::vector<T> >& X) {
    size_t nc = -1;
    for (auto x : X) {
        if (nc != -1) {
            assert(x.size() == nc);
        }
        nc = x.size();
    }
    size_t nr = X.size();

    std::vector<std::vector<T> > Xt;
    for (int j=0; j<nc; j++) {
        Xt.push_back(std::vector<T>(nr));
        std::vector<T>& v = Xt[j];
        for (int i=0; i<nr; i++) {
            v[i] = X[i][j];
        }
    }
    return Xt;
};

void 
transpose_stream(std::istream& input, std::ostream& output, 
        std::string tmpdir, 
        char delimiter='\t');

void
_transpose_stream(
        std::istream& input, 
        std::ostream& output,
        size_t n_threads=0,
        std::string tmpdir="/tmp",
        char delimiter='\t',
        std::string compression_program="lz4",
        size_t chunk_size=100,
        bool verbose=false);
}
