#pragma once

#include <string>
#include <vector>

#include <wrenlab/util/fs.hpp>
#include <wrenlab/util/progress_bar.hpp>

namespace wrenlab {

/* String manipulation */
std::string lowercase(std::string);
std::string uppercase(std::string);
bool startswith(const std::string, const std::string);
bool endswith(const std::string, const std::string);
std::vector<std::string> split(const std::string &text, char sep='\t');
std::string join(const std::string, const std::vector<std::string>);
std::string strip(std::string);
void replace_all(std::string&, const std::string&, const std::string&);

/* Subprocessing */
std::string exec(const char* cmd);
void subprocess_call(std::vector<std::string>);

};
