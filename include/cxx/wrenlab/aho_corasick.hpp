#pragma once

#include <string>
#include <vector>
#include <memory>

namespace wrenlab {
namespace AhoCorasick {

using namespace std;

class Node;
typedef std::vector<std::shared_ptr<Node> > node_vector;

class Node {
public:
	Node() {};
	Node(char c) {content = c; terminal = false;}
	string id;
	int depth();
	char content;		
	bool terminal;
	vector<shared_ptr<Node> > children;
	shared_ptr<Node> parent;
	shared_ptr<Node> fail;
	shared_ptr<Node> find(char c);
	shared_ptr<Node> find_or_fail(char c);
  std::shared_ptr<Node> search(char c);

  void build();

  node_vector descendants() {
    node_vector o;
    for (std::shared_ptr<Node> child : children) {
      o.push_back(child);
      for (std::shared_ptr<Node>& descendant : child->descendants()) {
        o.push_back(descendant);
      }
    }
    return o;
  }
};

struct node_ptr_comparator {
  inline bool operator()(const std::shared_ptr<Node>& n1, const std::shared_ptr<Node>& n2) {
    return n1->depth() < n2->depth();
  }
};

struct Match {
	string id;
	int start;
	int end;
	int length() const {return end - start;}

	bool overlaps(const Match& o) const {
		return (end <= o.start) && (start < o.end);
	}

  bool overlaps(size_t location) {
    return (start <= location) && (location <= end);
  }
};

std::vector<Match> longest_nonoverlapping_matches(std::vector<Match>&);

class Trie {
public:
	Trie(
        bool case_sensitive=false,
        bool remove_overlaps=false,
        std::string boundary_characters=""
    ) : 
        case_sensitive(case_sensitive),
        remove_overlaps(remove_overlaps)
        {
            root = std::shared_ptr<Node>(new Node());
            for (char c : boundary_characters) {
              this->boundary_characters.insert(c);
            }
        };

  std::vector<Match> search(std::string s);
	void add(string s);
	void add(string id, string s);
	void build();
private:
  size_t size;
  bool case_sensitive, remove_overlaps;
  std::set<char> boundary_characters; 
  std::shared_ptr<Node> root;
};

class MixedCaseSensitivityTrie {
  Trie ci, cs;
  bool remove_overlaps;

public:
  MixedCaseSensitivityTrie(bool remove_overlaps, std::string boundary_characters) : 
    ci(false, false, boundary_characters),
    cs(true, false, boundary_characters),
    remove_overlaps(remove_overlaps) {}

  void add (std::string id, std::string text, bool case_sensitive) {
    if (case_sensitive) {
      cs.add(id, text);
    } else {
      ci.add(id, text);
    }
  }

  void build() {
    cs.build();
    ci.build();
  }

  std::vector<Match> search(std::string& text) {
    std::vector<Match> matches = cs.search(text);
    for (Match m : ci.search(text)) {
      matches.push_back(m);
    }
    if (this->remove_overlaps) {
      matches = longest_nonoverlapping_matches(matches);
    }
    return matches;
  }
};



// end namespace
}
}
