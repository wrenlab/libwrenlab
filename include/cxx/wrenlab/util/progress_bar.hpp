#pragma once

#include <fstream>

namespace wrenlab {

class ProgressBar {
private:
    std::ostream* output;
    size_t current = 0;
    size_t maximum;
    size_t width = 40;
    void print();

public:
    ProgressBar(std::ostream*, size_t);
    void update(size_t);
    void finish();
};

};
