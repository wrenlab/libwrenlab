#pragma once

/* Forward iterator mixin class that iterates over items of type T.
 *
 * The subclass must implement:
 *  ValueT* next()
 *      Where next() returns a non-null pointer on success, and NULL
 *      at StopIteration. This pointer is required to be valid until
 *      next() is called again.
 */

namespace wrenlab {

template <typename T>
class Iterable {
public:
    struct forward_iterator {
        Iterable& obj;
        T* ptr;
        bool has_next = true;

    public:
        forward_iterator(Iterable& obj, T* ptr) : obj(obj), ptr(ptr) {}
        
        bool 
        operator==(forward_iterator const& o) const {
            if (o.ptr == NULL)
                return !has_next;
            return this->ptr == o.ptr;
        }

        bool 
        operator!=(forward_iterator const& o) const {
            if (o.ptr == NULL)
                return has_next;
            return this->ptr != o.ptr;
        }

        forward_iterator
        operator++() {
            this->ptr = *obj;
            has_next = ++obj;
            return *this;
        }

        T& operator*() {
            return *(this->ptr);
        }
    };

    virtual forward_iterator begin() {
        return forward_iterator(*this, **this);
    }

    virtual forward_iterator end() {
        return forward_iterator(*this, NULL);
    }

    virtual bool operator++();
    virtual T* operator*();
};

};

/* Example usage:

class Counter : public Iterable<size_t> {
    size_t current = 0;
    size_t max;

public:
    Counter(size_t max) {
        this->max = max;
    }

    Counter& operator++() {
        current++;
        return *this;
    }

    size_t* operator*() {
        if (current == max) {
            return NULL;
        }
        return &current;
    }
};

int main(int argc, char* argv[]) {
    Counter c(10);
    for (auto v : c) {
        std::cerr << v << std::endl;
    }
}
*/ 
