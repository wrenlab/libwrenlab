#pragma once

#include <cstdio>
#include <sys/stat.h>

#include <string>
#include <stdexcept>

#include <wrenlab/log.hpp>

namespace wrenlab {

std::string slurp(const std::string path);
std::string slurp(std::istream&);

bool file_exists(const std::string& path);
std::string file_path(int fd);
std::string file_path(FILE* file);
std::string make_temporary_file(const std::string prefix);
std::string get_default_temporary_directory();

class File {
protected:
    std::string m_path;
    struct stat m_stat;
    int stat_rc;

    virtual void refresh();

    File() {}

public:
    File(std::string);

    virtual std::string path() const { return m_path; }

    virtual std::string name();
    virtual bool exists();
    virtual bool is_file();
    virtual bool is_symlink();
    virtual bool is_directory();
    virtual void remove();

    bool 
    operator<(const File& o) const {
        return path() < o.path();
    }
};

class Directory : public File {
protected:
    Directory() : File() {}
public:
    Directory(std::string path) : File(path) {}

    std::vector<File> ls();
    static Directory create(std::string path);
};

class TemporaryDirectory : public Directory {
public:
    TemporaryDirectory();
    ~TemporaryDirectory();
};

class TemporaryFile : public File {
public:
    TemporaryFile() : File() {
        std::string prefix = get_default_temporary_directory();
        this->m_path = make_temporary_file(prefix);
    }

    ~TemporaryFile() {
        if (exists()) {
            unlink(this->m_path.c_str());
        }
    }
};
/*
class Pipe {
private:
    bool is_open;
    FILE* fp;
    int fd;
    std::string cmd, mode;

public:
    Pipe(std::string cmd, std::string mode) {
        this->cmd = cmd;
        this->mode = mode;
        fp = popen(cmd.c_str(), mode.c_str());
        fd = fileno(fp);
        this->is_open = true;
    }

    ~Pipe() {
        close();
    }

    void remove() {
        throw std::runtime_error("Cannot remove/unlink a pipe!");
    }

    void close() {
        if (is_open) {
            pclose(fp);
            is_open = false;
        }
    }

    std::string path() {
        return "/dev/fd/"+ std::to_string(fd);
    }
};

class NamedPipe : File {
public:
    NamedPipe() : File() {
        std::string prefix = get_default_temporary_directory();
        this->m_path = make_temporary_file(prefix);
        mkfifo(this->m_path.c_str(), 0666);
    }

    NamedPipe::~NamedPipe() {
        unlink(this->m_path.c_str());
    };
};
*/

};
