#pragma once

#include <wrenlab/util/cache.hpp>

namespace wrenlab {

Directory
get_download_cache_directory() {
  std::string path = get_cache_directory().path() + "/download";
  return Directory::create(path);
}

File download(std::string URL);

};
