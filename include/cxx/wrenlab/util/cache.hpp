#pragma once

#include <cstdlib>

#include <wrenlab/util/fs.hpp>

namespace wrenlab {

Directory
get_cache_directory() {
  std::string user = getenv("USER");
  std::string path = "/home/" + user + "/.cache/libwrenlab";
  return Directory::create(path);
}

};
