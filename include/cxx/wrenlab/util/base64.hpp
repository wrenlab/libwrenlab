#pragma once

#include <string>

namespace wrenlab {
namespace util {
namespace base64 {

std::string decode(std::string const&);
std::string encode(std::string const&);

};
};
};
