#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include <armadillo>

#include <wrenlab/matrix/io.hpp>
#include <wrenlab/matrix/types.hpp>
#include <wrenlab/sqlite.hpp>
#include <wrenlab/util.hpp>
#include <wrenlab/statistics/reduce.hpp>

namespace wrenlab {

class MatrixDB : public SQLiteDB {
protected:
    bool is_initialized;
    std::vector<std::string> _index, _columns;
    const char* SCHEMA = 
        "CREATE TABLE data ("
        "   i INTEGER PRIMARY KEY NOT NULL,"
        "   key VARCHAR UNIQUE,"
        "   data BLOB NOT NULL"
        ");"
        "CREATE TABLE data_t ("
        "   i INTEGER PRIMARY KEY NOT NULL,"
        "   key VARCHAR UNIQUE,"
        "   data BLOB NOT NULL"
        ");"
        "CREATE TABLE metadata ("
        "   key VARCHAR PRIMARY KEY NOT NULL,"
        "   value VARCHAR"
        ");";

    std::shared_ptr<SeriesIndex> r_ix, c_ix;

    std::string get_schema();

    void insert_vector_chunk(std::vector<Series>&, size_t, bool);
    size_t insert_vectors(SeriesReader&, size_t, bool);
    void load_indices(bool);

    std::vector<Vector*> get_vectors(const std::vector<std::string>&, bool);

    std::string get_compressed_data(const std::string&, bool);
    std::string 
    get_compressed_row(const std::string& key) {
        return get_compressed_data(key, true);
    }
    std::string 
    get_compressed_column(const std::string& key) {
        return get_compressed_data(key, false);
    }

    arma::mat
    create_matrix(std::vector<std::vector<double> >);
    arma::mat
    create_matrix(std::vector<Vector*>);

public:
    enum AXIS {
        ROW = 0,
        COLUMN = 1
    };

    enum REDUCTION2D {
        SUM = 0,
        MEAN = 1,
        MAX_MEAN = 2,
        PCA = 3
    };

    class Metadata {
    private:
        MatrixDB& db;
    public:
        Metadata(MatrixDB& db) : db(db) {}

        std::vector<std::string> keys() {
            std::vector<std::string> o;
            std::string sql = "SELECT key FROM metadata;";
            PreparedStatement ps = db.prepare_statement(sql);
            while (ps.has_next()) {
                o.push_back(ps.get_text(0));
            }
            return o;
        }

        void set(std::string key, std::string value) {
            std::string sql = "INSERT INTO metadata VALUES (?,?);";
            PreparedStatement ps = db.prepare_statement(sql);
            ps.bind_text(0, key);
            ps.bind_text(1, value);
            ps.step();
        }

        std::string get(std::string key) {
            std::string sql = "SELECT value FROM metadata WHERE key=?;";
            PreparedStatement ps = db.prepare_statement(sql);
            ps.bind_text(0, key);
            ps.step();
            if (ps.has_next()) {
                return ps.get_text(0);
            } else {
                throw std::runtime_error("Key not found in metadata table: '" 
                        + key + "'");
            }
        }
    };

    MatrixDB(const std::string path, bool read_only=true) 
            : SQLiteDB(path, read_only) {
        if (read_only) {
            assert(file_exists(path));
            load_indices(false);
        }
    }

    virtual void initialize(SeriesReader&, 
            std::string tmpdir="/tmp",
            bool transpose=false);

    virtual const std::vector<std::string>& columns();
    virtual const std::vector<std::string>& index();
    virtual void dump(std::ostream&);

    virtual size_t nrow() {
        return _index.size();
    }

    virtual size_t ncol() {
        return _columns.size();
    }

    virtual Vector* row(const std::string);
    virtual Vector* column(const std::string);

    virtual arma::vec arow(const std::string);
    virtual arma::vec acolumn(const std::string);

    virtual std::vector<Vector*>rows(const std::vector<std::string>& ix) {
        return get_vectors(ix, true);
    }

    virtual std::vector<Vector*>columns(const std::vector<std::string>& ix) {
        return get_vectors(ix, false);
    }

    // Return entire MatrixDB as an armadillo matrix
    virtual arma::mat data() {
        arma::mat o(nrow(), ncol());
        for (size_t i=0; i<nrow(); i++) {
            std::string key = _index[i];
            Vector* v = row(key);
            o.row(i) = arma::conv_to<arma::vec>::from(v->data).t();
            delete v;
        }
        return o;
    }

    arma::mat 
    slice(const std::vector<std::string>&, const std::vector<std::string>&);

    arma::vec
    collapse(std::vector<std::string>, AXIS, REDUCTION2D);

    /*
    arma::vec
    reduce(auto fn) {
        arma::vec o(ncol());
        for (size_t i=0; i<nrow(); i++) {
            Vector* row = this->row(_index[i]);
            arma::rowvec v = arma::conv_to<arma::rowvec>::from(row->data);
            o(i) = fn(v);
            delete row;
        }
        return o;
    }
    */
};

};
