#pragma once

#include <string>

namespace wrenlab {

std::string compress_string(const std::string& s);
std::string decompress_string(const std::string& s);

};
