#pragma once

#include <armadillo>

namespace wrenlab {
namespace statistics {

struct SVDResult {
    arma::mat U, S, V;

    arma::vec 
    distances(size_t i) {
        arma::mat dx = V.each_row() - V.row(i);
        return arma::pow(arma::sum(arma::pow(dx, 2), 1), 0.5);
    }

    arma::uvec
    nearest_neighbors(size_t i, size_t k) {
        arma::vec dx = distances(i);
        arma::uvec ix = arma::sort_index(dx);
        return ix.subvec(0, k-1);
    }
};

SVDResult randomized_svd(arma::mat& X, size_t rank);
SVDResult randomized_svd(arma::mat& X);

arma::mat
standardize(const arma::mat&);

};
};
