#pragma once

#include <string>
#include <vector>

#include <archive.h>
#include <archive_entry.h>

namespace wrenlab {

class TARReader {
protected:
    size_t block_size = 10240;
    size_t BUFFER_SIZE = 1024;
    char BUFFER[1024];
    std::string path;
    archive* tar;
    archive_entry* entry;
    int rc;
    bool _eof = false;
    bool is_open = true;

public:
    TARReader(std::string);
    ~TARReader();

    bool eof();
    std::string current_name();
    std::string data();
    std::vector<std::string> data_lines();
    void read_next();
    void close();
};

/* N.B.: creates "flat" tar files (i.e. strips paths, only handles files) */

class TARWriter {
private:
    std::string path;
    bool is_open = true;
    struct archive *a;
    struct archive_entry *entry;
    struct stat st;

public:
    TARWriter(std::string path, bool compress=true) {
        this->path = path;
        a = archive_write_new();
        if (compress) {
		    archive_write_add_filter_gzip(a);
            archive_write_set_option(a, "gzip", "compression-level", "1");
        }
		    archive_write_set_format_pax_restricted(a); // Note 1
        archive_write_open_filename(a, path.c_str());
    }

    ~TARWriter() {
        close();
    }

    void add(std::string path) {
        char buffer[8192];
        File file(path);
        if (!file.exists()) {
            std::string msg = "Attempting to add nonexistent file to TAR archive: " 
                + file.path();
            close();
            unlink(this->path.c_str());
            throw std::runtime_error(msg);
        }

        stat(path.c_str(), &st);
        if (st.st_size == 0) {
          LOG()->warn("Attempting to add empty file to TAR:", path);
          return;
        }
        entry = archive_entry_new();
        archive_entry_set_pathname(entry, file.name().c_str());
        archive_entry_set_size(entry, st.st_size);
        archive_entry_set_filetype(entry, AE_IFREG);
        archive_entry_set_perm(entry, 0644);
        archive_write_header(a, entry);

        int fd = open(path.c_str(), O_RDONLY);
        size_t length = read(fd, buffer, sizeof(buffer));
        while ( length > 0 ) {
            archive_write_data(a, buffer, length);
            length = read(fd, buffer, sizeof(buffer));
        }
        ::close(fd);
        archive_entry_free(entry);
    }

    void close() {
        if (is_open) {
            this->is_open = false;
            archive_write_close(a);
            archive_write_free(a);
        }
    }
};

};
