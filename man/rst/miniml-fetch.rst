=========
 miniml
=========

---------------------------------------------
Parse and manipulate NCBI GEO MiniML archives
---------------------------------------------

:Author: gilesc@omrf.org
:organization: Oklahoma Medical Research Foundation
:Address: 825 NE 13th Street
          Oklahoma City, OK 73104
	  Room MC103
:Date:   2018-12-14
:Copyright: public domain
:Version: 0.0.1
:Manual section: 1
:Manual group: text processing

SYNOPSIS
========

``miniml`` ``--help``

``miniml`` [ subcommand ] [ arguments ]


DESCRIPTION
===========

Fetch and save the MiniML archive for a given platform ID (GPL) from NCBI servers.

OPTIONS
=======

-o x   an option
-b     another
-f, --force
       really do it

For all other options see ``--help``.

EXAMPLES
===========

To fetch GPL96:

``miniml fetch 96 GPL96.tgz``

To fetch GPL570 into a subdirectory:

``miniml fetch 570 miniml/GPL570.tgz``
