=========
 miniml
=========

---------------------------------------------
Parse and manipulate NCBI GEO MiniML archives
---------------------------------------------

:Author: gilesc@omrf.org
:organization: Oklahoma Medical Research Foundation
:Address: 825 NE 13th Street
          Oklahoma City, OK 73104
	  Room MC103
:Date:   2018-12-14
:Copyright: public domain
:Version: 0.0.1
:Manual section: 1
:Manual group: text processing

SYNOPSIS
========

``miniml`` ``--help``

``miniml`` [ subcommand ] [ arguments ]


DESCRIPTION
===========

Parse and manipulate NCBI GEO MiniML archives.

Available subcommands:

- **dump** - dump the archive as an expression matrix of samples by probes

- **ptbl** - dump the platform description table (probe metadata)

- **cmap** - dump the mapping of probe indices to probe IDs for each sample

- **fetch** - fetch the archive from NCBI servers

For further information on subcommand arguments, run ``man miniml-<subcommand>``.
